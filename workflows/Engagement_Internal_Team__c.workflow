<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Team_Member_LOB_Engagement</fullName>
        <field>LOB__c</field>
        <formula>TEXT( Contact__r.Line_of_Business__c )</formula>
        <name>Team Member LOB Engagement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Team_Member_Office_Engagement</fullName>
        <field>Office__c</field>
        <formula>TEXT( Contact__r.Office__c )</formula>
        <name>Team Member Office Engagement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Team Member LOB Engagement</fullName>
        <actions>
            <name>Team_Member_LOB_Engagement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Contact__c &lt;&gt; NULL</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Team Member Office Engagement</fullName>
        <actions>
            <name>Team_Member_Office_Engagement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Contact__c &lt;&gt; NULL</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
