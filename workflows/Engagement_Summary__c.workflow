<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ENG_Summary_Approved_Email</fullName>
        <description>ENG Summary Approved Email</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>lmsilt__WorkflowNotifications/ENG_Summary_Approved</template>
    </alerts>
    <alerts>
        <fullName>ENG_Summary_Rejection_Email</fullName>
        <description>ENG Summary Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>lmsilt__WorkflowNotifications/ENG_Summary_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Store_Submitter_Eng_Summary</fullName>
        <field>Initial_Submitter__c</field>
        <formula>$User.Email</formula>
        <name>Store Submitter Eng Summary</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Summary_In_Process</fullName>
        <field>Summary_Status__c</field>
        <literalValue>In Process</literalValue>
        <name>Summary In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
