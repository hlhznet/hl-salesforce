<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Coverage_Team_Key</fullName>
        <field>Unique_Coverage_Team__c</field>
        <formula>Company__r.Full_Account_ID__c + &quot;|&quot; + Officer__r.Full_Contact_ID__c + &quot;|&quot; +
TEXT(Coverage_Type__c)</formula>
        <name>Unique Coverage Team Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Unique Coverage Team</fullName>
        <actions>
            <name>Unique_Coverage_Team_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
