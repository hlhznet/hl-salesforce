<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to aggregate the time record data to a single day.
The largest activity for the day is how all the time for the day will be categorized.
Should only have one record/client/user for each day</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Activity_Date__c</fullName>
        <externalId>false</externalId>
        <label>Activity Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Activity_Type__c</fullName>
        <externalId>false</externalId>
        <label>Activity Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Receive/Catalogue/Review Data</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conduct Research</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conduct Analysis</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Develop Work Product</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Deposition (including preparation for)</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Testimony (including preparation for)</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Discussions with Counsel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Vacation</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <externalId>false</externalId>
        <formula>Hourly_Rate__c *  Hours_Worked__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billable_Staff_Initials__c</fullName>
        <description>Used for reporting/dashboard</description>
        <externalId>false</externalId>
        <formula>Time_Record_Period_Staff_Member__r.Contact__r.Initials__c + &apos; - &apos; +  IF(Is_Billable__c, &apos;Chargeable&apos;, &apos;Non-Charge&apos;)</formula>
        <label>Billable Staff Initials</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Day_Of_Week__c</fullName>
        <description>(0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday)</description>
        <externalId>false</externalId>
        <formula>MOD(Activity_Date__c - DATE(1900, 1, 7), 7)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Day Of Week</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Engagement</label>
        <referenceTo>Engagement__c</referenceTo>
        <relationshipLabel>Time Record Rollup Days</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Days</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Hourly_Rate__c</fullName>
        <externalId>false</externalId>
        <label>Hourly Rate</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Hours_Worked_Billable__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Is_Billable__c,Hours_Worked__c,0.0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Hours Worked Billable</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hours_Worked_Non_Billable__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Is_Billable__c, 0.0, Hours_Worked__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Hours Worked Non Billable</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hours_Worked__c</fullName>
        <externalId>false</externalId>
        <label>Hours Worked</label>
        <precision>3</precision>
        <required>false</required>
        <scale>1</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Billable__c</fullName>
        <description>Indicates if the time is billable</description>
        <externalId>false</externalId>
        <formula>IF(Special_Project__c != null, Special_Project__r.Is_Billable__c, true)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Billable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Chargeable__c</fullName>
        <description>Text Indication of Chargeable vs Non Chargeable</description>
        <externalId>false</externalId>
        <formula>IF(Special_Project__c != null, IF(Special_Project__r.Is_Billable__c,&apos;Chargeable&apos;,&apos;Non-Chargeable&apos;), &apos;Chargeable&apos;)</formula>
        <label>Is Chargeable</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Key__c</fullName>
        <description>Record key - combines Activity Date, User and Related Object Id</description>
        <externalId>true</externalId>
        <label>Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_Name__c</fullName>
        <externalId>false</externalId>
        <formula>CASE(MONTH(Activity_Date__c), 1, &quot;01. Jan&quot;,2, &quot;02. Feb&quot;,
3, &quot;03. Mar&quot;,
4, &quot;04. Apr&quot;,
5, &quot;05. May&quot;,
6, &quot;06. Jun&quot;,
7, &quot;07. Jul&quot;,
8, &quot;08. Aug&quot;,
9, &quot;09. Sep&quot;,
10, &quot;10. Oct&quot;,
11, &quot;11. Nov&quot;,
12, &quot;12. Dec&quot;, &quot;None&quot;)</formula>
        <label>Month Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity__c</referenceTo>
        <relationshipLabel>Time Record Rollup Days</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Days</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project_Name__c</fullName>
        <externalId>false</externalId>
        <formula>BLANKVALUE(BLANKVALUE(Opportunity__r.Name, Engagement__r.Name),Special_Project__r.Name)</formula>
        <label>Project Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Special_Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Special Project</label>
        <referenceTo>Special_Project__c</referenceTo>
        <relationshipLabel>Time Record Rollup Days</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Days</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Staff_Member_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Time_Record_Period_Staff_Member__r.Contact__r.Contact_Full_Name__c</formula>
        <label>Staff Member Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Staff_Title__c</fullName>
        <externalId>false</externalId>
        <label>Staff Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Record_Period_Staff_Member__c</fullName>
        <description>Related time record period staff member record</description>
        <externalId>false</externalId>
        <label>Time Record Period Staff Member</label>
        <referenceTo>Time_Record_Period_Staff_Member__c</referenceTo>
        <relationshipLabel>Time Record Rollup Days</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Days</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Time Record Rollup Day</label>
    <nameField>
        <displayFormat>TRRD-{00000000}</displayFormat>
        <label>Time Record Rollup Day Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Time Record Rollup Days</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
