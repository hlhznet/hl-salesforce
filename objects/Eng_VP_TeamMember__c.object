<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is related with Engagement Valuation Period Position and HL Internal Team Members</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Engagement_VP_Position__c</fullName>
        <externalId>false</externalId>
        <label>Engagement Valuation Period Position</label>
        <referenceTo>Eng_VP_Position__c</referenceTo>
        <relationshipLabel>Eng Valuation Period TeamMembers</relationshipLabel>
        <relationshipName>Eng_VP_TeamMembers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Is_Team_Member_Deactivated_Once__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field is to determine when related Opp/Eng Team Member was removed from Opp/Eng once.</description>
        <externalId>false</externalId>
        <label>Is Team Member Deactivated Once</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <externalId>false</externalId>
        <label>Role</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Analyst</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Associate</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Manager</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Principal</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Industry</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Staff__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Staff</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Eng_PV_TeamMembers</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Team_Member_Status__c</fullName>
        <externalId>false</externalId>
        <formula>IF( End_Date__c &lt;=  TODAY()  , &apos;Inactive&apos;, &apos;Active&apos;)</formula>
        <label>Team Member Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Eng Valuation Period TeamMember</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Eng Valuation Period TeamMembers</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>HL_Eng_VP_Position_Change_Not_Allowed</fullName>
        <active>true</active>
        <description>To stop changing Engagement VP Position on Engagement VP Team Member.</description>
        <errorConditionFormula>IF(NOT(ISNEW()),IF(ISCHANGED(Engagement_VP_Position__c), true, false), false)</errorConditionFormula>
        <errorDisplayField>Engagement_VP_Position__c</errorDisplayField>
        <errorMessage>You are not allowed to change &apos;Engagement VP Position&apos;.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>HL_Eng_VP_TM_Name_Change_Not_Allowed</fullName>
        <active>true</active>
        <description>To stop changing Name on VP Team Member</description>
        <errorConditionFormula>IF(NOT(ISNEW()),IF(AND(ISCHANGED(Name),(PRIORVALUE(Name) ==   Staff__r.FirstName+&apos; &apos;+Staff__r.LastName)), true, false), false)</errorConditionFormula>
        <errorDisplayField>Name</errorDisplayField>
        <errorMessage>You are not allowed to change Name.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>HL_Eng_VP_TM_Staff_Change_Not_Allowed</fullName>
        <active>true</active>
        <description>To stop changing staff on VP Team Member</description>
        <errorConditionFormula>IF(NOT(ISNEW()),IF(ISCHANGED(Staff__c), true, false), false)</errorConditionFormula>
        <errorDisplayField>Staff__c</errorDisplayField>
        <errorMessage>You are not allowed to change Staff.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Start_Date</fullName>
        <active>true</active>
        <description>Start Date is mandatory field</description>
        <errorConditionFormula>ISBLANK(Start_Date__c)</errorConditionFormula>
        <errorMessage>Start Date is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Start_Date_End_Validation</fullName>
        <active>true</active>
        <description>To keep start date always equal or less than end date.</description>
        <errorConditionFormula>IF( Start_Date__c&gt; End_Date__c , true, false)</errorConditionFormula>
        <errorMessage>Start date cannot be after the end date.</errorMessage>
    </validationRules>
</CustomObject>
