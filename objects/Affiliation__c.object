<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom object to track a contact to account relationship, where the contact is directly related to the account.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Affiliated Contacts</relationshipLabel>
        <relationshipName>Affiliations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Affiliation_Title__c</fullName>
        <externalId>false</externalId>
        <label>Affiliation Title</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Committees__c</fullName>
        <externalId>false</externalId>
        <label>Committees</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Audit</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Audit-Chair</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Audit-Member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Compensation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Executive</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Finance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Governance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Nominating</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Outside Counsel</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Contact_Name__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/?sort=&quot; &amp; Contact__r.LastName&amp; &quot;,&quot; &amp; Contact__r.FirstName, &quot;&quot;, &quot;_parent&quot;) &amp;
HYPERLINK(&quot;/&quot;&amp;Contact__r.Id , Contact__r.LastName+&quot;, &quot;+Contact__r.FirstName, &apos;_parent&apos;)</formula>
        <label>Contact Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Affiliated Companies</relationshipLabel>
        <relationshipName>Affiliations</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Notes</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Legacy_SLX_ID__c</fullName>
        <externalId>true</externalId>
        <label>Legacy SLX ID</label>
        <length>12</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status_Type__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT( Status__c ) + &quot; - &quot; + TEXT( Type__c )</formula>
        <label>Status - Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Current</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Former</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <externalId>false</externalId>
        <label>Title</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Owner (Less than 10%)</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Consultant</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>NULL</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director of Alumni Development</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Passive member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member of board of directors</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chairman</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chief Executive Officer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chief Financial Officer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Investor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>President/Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director - advisory board</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board of directors - Treasurer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Treasurer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Independent director</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Founder and board member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Owned by husband. Single member LLC. I am not an owner.</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Shareholder</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>LLC member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Principal</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board Director</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Leadership board member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board of advisors member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Chairman of the Board</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Advisory Board President</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Research Analyst</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director, chair of audit committee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board appointee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>President, board member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director; Member of Exec Committee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Participant on Advisory Board as an Advisor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member of the board</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board president</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member/Co-portfolio manager</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Limited Partner/Member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>CEO</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board of Directors</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Shareholder, board seat</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board of advisors</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Board of directors, shareholder</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Advisory board member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member; VP, membership</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member, board of directors and executive committee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Manager</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Trustee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member of board, secretary</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conference producer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member, board of advisors</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Director, Board of Directors</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Member, board of trustees</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Corporate Director</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Head of Finance</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Accountant</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Attorney</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Charity</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Consultant</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Former Employee</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inside Board Member</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other Advisor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other Employer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Outside Board Member</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Affiliation</label>
    <nameField>
        <displayFormat>A-{000000}</displayFormat>
        <label>Affiliation Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Affiliations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Affiliation_Contact</fullName>
        <active>true</active>
        <errorConditionFormula>Contact__r.RecordType.DeveloperName = &apos;Houlihan_Employee&apos; &amp;&amp;
 ($Profile.Name != &apos;Compliance User&apos; &amp;&amp;
 $Profile.Name != &apos;System Administrator&apos;)</errorConditionFormula>
        <errorDisplayField>Contact__c</errorDisplayField>
        <errorMessage>Only Compliance can add HL Employees as affiliations.</errorMessage>
    </validationRules>
</CustomObject>
