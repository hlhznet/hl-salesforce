<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Time Record Period Staff Members</relationshipLabel>
        <relationshipName>Time_Record_Period_Staff_Members</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <externalId>false</externalId>
        <formula>User__r.Email</formula>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_Name_End__c</fullName>
        <description>Month Name Based on the End Date of the Time Record Period</description>
        <externalId>false</externalId>
        <formula>CASE(MONTH(Time_Record_Period__r.End_Date__c ), 1, &quot;01. Jan&quot;,2, &quot;02. Feb&quot;, 
3, &quot;03. Mar&quot;, 
4, &quot;04. Apr&quot;, 
5, &quot;05. May&quot;, 
6, &quot;06. Jun&quot;, 
7, &quot;07. Jul&quot;, 
8, &quot;08. Aug&quot;, 
9, &quot;09. Sep&quot;, 
10, &quot;10. Oct&quot;, 
11, &quot;11. Nov&quot;, 
12, &quot;12. Dec&quot;, &quot;None&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Month Name End</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Month_Name_Start__c</fullName>
        <description>Month Name Based on the Start Date of the Time Record Period</description>
        <externalId>false</externalId>
        <formula>CASE(MONTH(Time_Record_Period__r.Start_Date__c ), 1, &quot;01. Jan&quot;,2, &quot;02. Feb&quot;,
3, &quot;03. Mar&quot;,
4, &quot;04. Apr&quot;,
5, &quot;05. May&quot;,
6, &quot;06. Jun&quot;,
7, &quot;07. Jul&quot;,
8, &quot;08. Aug&quot;,
9, &quot;09. Sep&quot;,
10, &quot;10. Oct&quot;,
11, &quot;11. Nov&quot;,
12, &quot;12. Dec&quot;, &quot;None&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Month Name Start</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Office__c</fullName>
        <description>Staff member&apos;s office at time of creation</description>
        <externalId>false</externalId>
        <label>Office</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Phone__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.Phone</formula>
        <label>Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Record_Period__c</fullName>
        <description>Related time record period record</description>
        <externalId>false</externalId>
        <label>Time Record Period</label>
        <referenceTo>Time_Record_Period__c</referenceTo>
        <relationshipLabel>Time Record Period Staff Members</relationshipLabel>
        <relationshipName>Time_Record_Period_Staff_Members</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <description>Staff Member&apos;s Title at time of Creation</description>
        <externalId>false</externalId>
        <label>Title</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Hours_FR__c</fullName>
        <description>Total Hours for the Time Period for FR (Weekday + Weekend)</description>
        <externalId>false</externalId>
        <label>Total Hours FR</label>
        <summarizedField>Time_Record_Rollup_Week__c.Total_Hours_Worked__c</summarizedField>
        <summaryForeignKey>Time_Record_Rollup_Week__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Friday__c</fullName>
        <description>Total Hours Friday</description>
        <externalId>false</externalId>
        <label>Total Hours Friday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>5</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Litigation__c</fullName>
        <description>Total hours recorded for the period</description>
        <externalId>false</externalId>
        <label>Total Hours Litigation</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Monday__c</fullName>
        <description>Total Hours Monday</description>
        <externalId>false</externalId>
        <label>Total Hours Monday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>1</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Saturday__c</fullName>
        <description>Total Hours Saturday</description>
        <externalId>false</externalId>
        <label>Total Hours Saturday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>6</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Sunday__c</fullName>
        <description>Total Sunday Hours</description>
        <externalId>false</externalId>
        <label>Total Hours Sunday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>0</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Thursday__c</fullName>
        <description>Total Hours Thursday</description>
        <externalId>false</externalId>
        <label>Total Hours Thursday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>4</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Tuesday__c</fullName>
        <description>Total Hours Tuesday</description>
        <externalId>false</externalId>
        <label>Total Hours Tuesday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>2</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Wednesday__c</fullName>
        <description>Total Hours Wednesday</description>
        <externalId>false</externalId>
        <label>Total Hours Wednesday</label>
        <summarizedField>Time_Record__c.Hours_Worked__c</summarizedField>
        <summaryFilterItems>
            <field>Time_Record__c.Day_Of_Week__c</field>
            <operation>equals</operation>
            <value>3</value>
        </summaryFilterItems>
        <summaryForeignKey>Time_Record__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Weekday_FR__c</fullName>
        <description>Total weekday hours for the period</description>
        <externalId>false</externalId>
        <label>Total Hours Weekday FR</label>
        <summarizedField>Time_Record_Rollup_Week__c.Hours_Worked_Weekday__c</summarizedField>
        <summaryForeignKey>Time_Record_Rollup_Week__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total_Hours_Weekend_FR__c</fullName>
        <description>Total Weekend Hours for the Time Period for FR</description>
        <externalId>false</externalId>
        <label>Total Hours Weekend FR</label>
        <summarizedField>Time_Record_Rollup_Week__c.Hours_Worked_Weekend__c</summarizedField>
        <summaryForeignKey>Time_Record_Rollup_Week__c.Time_Record_Period_Staff_Member__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Related user record</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Time_Record_Period_Staff_Members</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Time Record Period Staff Member</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Time_Record_Period__c</columns>
        <columns>NAME</columns>
        <columns>Contact__c</columns>
        <columns>Email__c</columns>
        <columns>Title__c</columns>
        <columns>User__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>TRPSM-{00000000}</displayFormat>
        <label>Time Record Period Staff Member Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Time Record Period Staff Members</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
