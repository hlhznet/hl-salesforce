<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>JUNCTION OBJECT: Custom object to track additional clients and subjects for an engagement.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Client_Subject__c</fullName>
        <externalId>false</externalId>
        <label>Client/Subject</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Engagement_Clients_Subjects</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Engagement_Client_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Engagement__r.Client__r.Name</formula>
        <label>Engagement Client Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Engagement__r.Name</formula>
        <label>Engagement Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Status__c</fullName>
        <externalId>false</externalId>
        <formula>Engagement__r.Status__c</formula>
        <label>Engagement Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Subject_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Engagement__r.Subject__r.Name</formula>
        <label>Engagement Subject Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement__c</fullName>
        <externalId>false</externalId>
        <label>Engagement</label>
        <referenceTo>Engagement__c</referenceTo>
        <relationshipLabel>Additional Clients/Subjects</relationshipLabel>
        <relationshipName>Engagement_Clients_Subjects</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Legacy_SLX_Id__c</fullName>
        <externalId>true</externalId>
        <label>Legacy SLX Id</label>
        <length>12</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Loan_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Loan Amount</label>
        <precision>14</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Related_Object_Id__c</fullName>
        <description>Use to store the indirect relationship to another object based on the &apos;type&apos; field.</description>
        <externalId>false</externalId>
        <label>Other Related Object Id</label>
        <length>25</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Percent_Ownership__c</fullName>
        <externalId>false</externalId>
        <label>Percent Ownership</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Primary_REPORTING__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Primary__c, &apos;True&apos;, &apos;False&apos;)</formula>
        <label>Primary (REPORTING)</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Primary__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>The primary client or subject defined on the engagement level.</inlineHelpText>
        <label>Primary</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <description>This is equivalent to the Account Position from SalesLogix</description>
        <externalId>false</externalId>
        <label>Role</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Adverse</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Buyer Contact</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Buyer Financial Advisor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Buyer Legal Counsel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Equity Holder</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Financial Advisor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Legal Counsel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Lender</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Majority Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Management Contact</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Minority Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Post-Transaction</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Pre-Transaction</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Primary Contact</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Seller Legal Counsel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Seller Majority Owner</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Seller Management Contact</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Client</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Counterparty</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Equity Holder</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Lender</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>PE Firm</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Subject</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>View_Details__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(Id, &quot;View Details&quot;,&quot;_parent&quot;)</formula>
        <label>View Details</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Engagement Client Subject</label>
    <nameField>
        <displayFormat>REL-{000000}</displayFormat>
        <label>Record Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Engagement Clients Subjects</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>EngClientSubject</fullName>
        <active>true</active>
        <label>EngClientSubject</label>
        <picklistValues>
            <picklist>Role__c</picklist>
            <values>
                <fullName>Adverse</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Financial Advisor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equity Holder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Financial Advisor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lender</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Majority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Management Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Minority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Post-Transaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Transaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Primary Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Majority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Management Contact</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Client</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Counterparty</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equity Holder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lender</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PE Firm</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Subject</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>EngClientSubject_Other</fullName>
        <active>true</active>
        <label>EngClientSubject Other</label>
        <picklistValues>
            <picklist>Role__c</picklist>
            <values>
                <fullName>Adverse</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Financial Advisor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Buyer Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equity Holder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Financial Advisor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lender</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Majority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Management Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Minority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Post-Transaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pre-Transaction</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Primary Contact</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Legal Counsel</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Majority Owner</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Seller Management Contact</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Client</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Counterparty</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Equity Holder</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Lender</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>PE Firm</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Subject</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Primary_Cannot_Change_Type_Eng</fullName>
        <active>true</active>
        <description>Primary Client/Subject Cannot change Type</description>
        <errorConditionFormula>ISCHANGED( Type__c ) &amp;&amp; Primary__c</errorConditionFormula>
        <errorMessage>Primary Client/Subjects cannot change Type.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Additional_Client</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Additional Client</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a0T/e?
CF00Ni000000D9DbX={!Engagement__c.Name}&amp;
CF00Ni000000D9DbX_lkid={!Engagement__c.Id}&amp;
00Ni000000D9Dbh=Client&amp;
saveURL=/{!Engagement__c.Id}&amp;
retURL=/{!Engagement__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Additional_Other</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Additional Other</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a0T/e?
RecordType=012i0000001NRQD&amp;
00Ni000000D9Dbh=Other&amp;
CF00Ni000000D9DbX={!Engagement__c.Name}&amp;
CF00Ni000000D9DbX_lkid={!Engagement__c.Id}&amp;
saveURL=/{!Engagement__c.Id}&amp;
retURL=/{!Engagement__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Additional_Subject</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>Additional Subject</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a0T/e?
00Ni000000D9Dbh=Subject&amp;
retURL=/{!Engagement__c.Id}&amp;
saveURL=/{!Engagement__c.Id}&amp;
CF00Ni000000D9DbX={!Engagement__c.Name}&amp; 
CF00Ni000000D9DbX_lkid={!Engagement__c.Id}</url>
    </webLinks>
</CustomObject>
