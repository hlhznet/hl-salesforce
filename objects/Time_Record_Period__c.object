<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Time record periods</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>End Date for the Period</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Is_Locked__c</fullName>
        <externalId>false</externalId>
        <formula>TODAY() -  Start_Date__c &gt; 14</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Locked</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Period_Category__c</fullName>
        <description>Used to categorize the period to allow for multiple functionalities</description>
        <externalId>false</externalId>
        <label>Period Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Beta</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>FR</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Litigation</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>PV</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>TFR</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Public_Group__c</fullName>
        <description>The time tracking public group name</description>
        <externalId>false</externalId>
        <formula>CASE(TEXT(Period_Category__c), &quot;PV&quot;, &quot;Time_Tracking_PV&quot;, &quot;FR&quot;, &quot;Time_Tracking_FR&quot;, &quot;Beta&quot;, &quot;Time_Tracking_Beta&quot;, &quot;Litigation&quot;, &quot;Time_Tracking_Litigation&quot;, &quot;TFR&quot;, &quot;Time_Tracking_TFR&quot;, null)</formula>
        <label>Public Group</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>Period Start date</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Year__c</fullName>
        <description>Reporting Year</description>
        <externalId>false</externalId>
        <formula>TEXT(YEAR( Start_Date__c ))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Year</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Time Record Period</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Public_Group__c</columns>
        <columns>Period_Category__c</columns>
        <columns>Start_Date__c</columns>
        <columns>End_Date__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>TRP-{00000000}</displayFormat>
        <label>Time Record Period Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Time Record Periods</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Period_Category__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End_Date__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
</CustomObject>
