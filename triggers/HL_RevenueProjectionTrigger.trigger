trigger HL_RevenueProjectionTrigger on Revenue_Projection__c (before insert, before update, after insert, after update) {
	HL_RevenueProjectionHandler revenueProjectionHandler = new HL_RevenueProjectionHandler();
    if(Trigger.IsBefore && Trigger.IsInsert) {
        revenueProjectionHandler.onBeforeInsert(Trigger.new);
    }
	
    if(Trigger.IsBefore && Trigger.IsUpdate) {
        revenueProjectionHandler.onBeforeUpdate(Trigger.new);
    }
    
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
        revenueProjectionHandler.onAfterInsert(Trigger.newMap);
    }  
}