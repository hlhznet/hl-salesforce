trigger HL_OpportunityApprovalTrigger on Opportunity_Approval__c (before insert, before update) {
    if(Trigger.IsBefore)
    {
        if(Trigger.IsInsert)
        {
            //Get Map of Subject Id with Related Transaction Text (including when Client)
            Map<Id, String> cse = HL_OpportunityApprovalHandler.GetRelatedTransactionData(Trigger.New, True);
            //Get Map of Subject Id with Related Transaction Text (subject only)
            Map<Id, String> se = HL_OpportunityApprovalHandler.GetRelatedTransactionData(Trigger.New, True);
            //Assign the Related Transaction (Previous Transaction) Text
            for(Opportunity_Approval__c oa : Trigger.New)
            {
                if(oa.Job_Type__c == 'Buyside') //For Buyside we want to include client engagements
                    oa.Previous_Transactions__c = cse.get(oa.Subject_Company_ID__c);
                else
                    oa.Previous_Transactions__c = se.get(oa.Subject_Company_ID__c);
            }
        }
        
        if(Trigger.IsInsert || Trigger.IsUpdate){
            HL_OpportunityApprovalHandler.UpdateFormApproved(Trigger.New);
        }
    }
}