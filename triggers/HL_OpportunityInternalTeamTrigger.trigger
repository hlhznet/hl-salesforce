trigger HL_OpportunityInternalTeamTrigger on Opportunity_Internal_Team__c (before update, before delete, after insert, after update, after delete) {
    if(Trigger.isAfter)
    {
        HL_OpportunityInternalTeamHandler handler = new HL_OpportunityInternalTeamHandler(Trigger.isExecuting, Trigger.size);

        if(Trigger.isInsert)
            handler.onAfterInsert(Trigger.New);
        else if(Trigger.isUpdate)
            handler.onAfterUpdate(Trigger.oldMap, Trigger.newMap, Trigger.New);
        else if(Trigger.isDelete){
            handler.onAfterDelete(Trigger.oldMap, Trigger.Old);
        }
    }

    //Audit Tracking
    if(Trigger.IsBefore && (Trigger.IsUpdate || Trigger.IsDelete) &&
       HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Audit_Opportunity_Internal_Team))
    {
        HL_AuditRecordHandler auditHandler = new HL_AuditRecordHandler(SObjectType.Opportunity_Internal_Team__c.getSobjectType());
        auditHandler.RecordAudit(Trigger.oldMap, Trigger.newMap);
    }
}