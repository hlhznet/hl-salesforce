trigger HL_EngagementExternalTeamTrigger on Engagement_External_Team__c (before insert, before update, before delete) {
    if(HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Engagement_External_Team))
    {
        HL_EngagementExternalTeamHandler handler;

        if(Trigger.IsBefore){
            if(Trigger.IsDelete){
                handler = new HL_EngagementExternalTeamHandler(Trigger.Old);
                handler.RemovePrimaryEET();
            }
            else{
                handler = new HL_EngagementExternalTeamHandler(Trigger.New);
                handler.UpdateEETUnique();
                handler.UpdatePrimaryEngagementContact();
                if(Trigger.isInsert && !SL_CheckRecursive.skipOnConvert)
                    handler.UpdateSortFields();
            }
        }
    }

    //Audit Tracking
    if(Trigger.IsBefore && (Trigger.IsUpdate || Trigger.IsDelete) &&
       HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Audit_Engagement_External_Team))
    {
        HL_AuditRecordHandler auditHandler = new HL_AuditRecordHandler(SObjectType.Engagement_External_Team__c.getSobjectType());
        auditHandler.RecordAudit(Trigger.oldMap, Trigger.newMap);
    }
}