({
	doInit : function(component, event) {
        var action = component.get("c.GetById");
        var eventId = component.get("v.activityId");
        var primaryId;
        action.setParams({eventId: eventId});
        action.setCallback(this, function(a) {
            var eventRecord = a.getReturnValue();
            component.set("v.activity", eventRecord);
            component.set("v.activityType", eventRecord.Type);
            primaryId = eventRecord.ParentId__c == null ? eventId : eventRecord.ParentId__c;
            component.set("v.primaryActivityId", primaryId);
            var deleteAction = component.get("c.HasDeleteRights");
            var attendeesAction = component.get("c.GetAttendees");
            var employeesAction = component.get("c.GetEmployees");
            var companiesAction = component.get("c.GetComps");
            var oppsAction = component.get("c.GetOpps");
            deleteAction.setParams({"eventId":primaryId});
            attendeesAction.setParams({"eventId":primaryId});
            employeesAction.setParams({"eventId":primaryId});
            companiesAction.setParams({"eventId":primaryId});
            oppsAction.setParams({"eventId":primaryId});
            
            deleteAction.setCallback(this,function(da){component.set("v.deleteEnabled",da.getReturnValue()); });
            attendeesAction.setCallback(this,function(aa){component.set("v.relatedAttendees",aa.getReturnValue()); });
            employeesAction.setCallback(this,function(ea){component.set("v.relatedEmployees",ea.getReturnValue()); });
            companiesAction.setCallback(this,function(ca){component.set("v.relatedCompanies",ca.getReturnValue()); });
            oppsAction.setCallback(this,function(oa){component.set("v.relatedOpps",oa.getReturnValue()); });
            $A.enqueueAction(deleteAction);
            $A.enqueueAction(attendeesAction);
            $A.enqueueAction(employeesAction);
            $A.enqueueAction(companiesAction);
            $A.enqueueAction(oppsAction);
        });
        $A.enqueueAction(action);
        
         //Get the Activity Supplement Record
        var activitySupplementAction = component.get("c.GetActivitySupplement");
        activitySupplementAction.setParams({eventId:eventId});
        activitySupplementAction.setCallback(this, function(response){
            console.log('Response');
            console.log(response.getReturnValue());
            component.set("v.activitySupplement", response.getReturnValue());                                 
        });       
        $A.enqueueAction(activitySupplementAction);
    },
    cancelSendEmail : function(component){
        component.set("v.isEmailing", false);
    },
    cancelEditActivity : function(component){
      	component.set("v.isEditing", false);
    },
    confirmDelete : function(component, event){
        if(confirm('Are you sure?')){
            var eventRecord = component.get("v.activity");
            var primaryId = (eventRecord.ParentId__c == null) ? eventRecord.Id : eventRecord.ParentId__c;
            var action = component.get("c.DeleteRecord");
            action.setParams({"id":primaryId});
            action.setCallback(this,function(response){           
                var navAction = $A.get("e.force:navigateToComponent");
                navAction.setParams({
                    componentDef: "c:MyActivities"
                });
                navAction.fire();
            });
            $A.enqueueAction(action);
        }
    },
    emailSent : function(component, event){
        component.set("v.isEmailing", false);
    },
    navigateToEdit : function(component, event){
        //var action = $A.get("e.force:navigateToComponent");
        //var eventId = component.get("v.activityId");
        //action.setParams({
            //componentDef: "c:EditActivity",
            //componentAttributes: {
                   // activityId:eventId   
                //}
        //});
        //action.fire();
        component.set("v.isEditing", true);
    },
    navigateToEmail : function(component, event){
      	//var action = $A.get("e.force:navigateToComponent");
        //var eventId = component.get("v.activityId");
        //action.setParams({
            //componentDef: "c:SendEmail",
            //componentAttributes: {
                    //templateId:'Event',
                    //relatedId:eventId   
                //}
        //});
        //action.fire();
        component.set("v.isEmailing", true);
    },
    navigateToMyActivities : function(component, event){
        var action = $A.get("e.force:navigateToComponent");
        action.setParams({
            componentDef: "c:MyActivities"
        });
        action.fire();
    }
})