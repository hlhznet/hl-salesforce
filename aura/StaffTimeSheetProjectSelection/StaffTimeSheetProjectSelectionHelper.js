({
	onProjectSelected: function(component) {
		var projectControl = component.find("project");
		var selectionEvent = component.getEvent("projectSelectionEvent");

		//Fire the Selection Event
		selectionEvent.setParams({
			"selectedId": projectControl.get("v.value")
		});
		selectionEvent.fire();
	}
})