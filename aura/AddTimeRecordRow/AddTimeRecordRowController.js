({
	addRecord: function(component, event, helper) {
		helper.addTimeRecord(component, helper);
	},
	onProjectChanged: function(component, event, helper) {
		helper.handleProjectChanged(component, event, helper);
	}
})