@isTest
private class Test_HL_OpportunityHandler {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        List<Opportunity__c> opps = SL_TestSetupUtils.CreateOpp('', 6);
        insert opps;
        Engagement__c e = SL_TestSetupUtils.CreateEngagement('', 1)[0];
        e.Opportunity__c = opps[0].Id;
        
        Test.startTest();
        	insert e;
        
            //Update for Comments
            opps[0].Opportunity_Comments__c = 'Test Comment Longer ';
            update opps[0];
        	
        	//Test Long Comment
        	for(integer x=0; x<20; x++)
                opps[0].Opportunity_Comments__c += 'Test Comment Longer ';
        
            //Change DND Status 
            opps[0].DND_Status__c = 'APPROVED';
            update opps[0];    
        Test.stopTest();
        
        Opportunity__c o = [SELECT DND_Status__c, Opportunity_Comments__c FROM Opportunity__c WHERE ID =: opps[0].Id];
        
        //Validate the Opportunity Comments have been Cleared
        System.assert(String.isBlank(o.Opportunity_Comments__c));
    }
    
    @isTest 
	static void TestOpportunityJoinerCreations() 
	{
        //Setup Test Data
		Account objClientAccount = new Account(Name='Test Client Account');
		insert objClientAccount;
		
		Account objClientAccountToUpdate = new Account(Name='Test Client Account');
		insert objClientAccountToUpdate;

		Account objSubjectAccount = new Account(Name='Test Subject Account');
		insert objSubjectAccount;

		Opportunity__c objOpportunityNew = new Opportunity__c(Name='Test Opportunity', Client__c = objClientAccount.Id, 
															  Subject__c = objSubjectAccount.Id, Stage__c = 'Pitched',
															  Line_of_Business__c = 'CF', Job_Type__c = 'Financing',
															  Primary_Office__c = 'AT');
        
        Test.startTest();
			insert objOpportunityNew;
        	List<Opportunity_Client_Subject__c> ocsAfterInsertResults = [Select Id From Opportunity_Client_Subject__c where Opportunity__c = :objOpportunityNew.Id];
        	objOpportunityNew.Name = 'dndOpportunity';
			objOpportunityNew.Client__c = objClientAccountToUpdate.Id;
        	update objOpportunityNew;
        Test.stopTest();
        
		//check whether 2 client subject records are created
		system.AssertEquals(ocsAfterInsertResults.Size(), 2);

		// check whether the previous combination of joiner record is deleted, Now it should be deleted
		//system.AssertEquals([Select Id From Opportunity_Client_Subject__c where Opportunity__c = :objOpportunityNew.Id AND Client_Subject__c = :objClientAccount.Id].Size(), 0);
		// check whether the updated client Account is created in the Joiner
		//system.AssertEquals([Select Id From Opportunity_Client_Subject__c where Opportunity__c = :objOpportunityNew.Id].Size(), 2);
	}
}