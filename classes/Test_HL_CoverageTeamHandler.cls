@isTest
private class Test_HL_CoverageTeamHandler {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Account a = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert a;
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE)[0];
        insert c;
        List<Coverage_Team__c> ctList = new List<Coverage_Team__c>();
        ctList.add(new Coverage_Team__c(Company__c = a.Id, Officer__c = c.Id));
        insert ctList;          
        a = [SELECT Coverage_Team_Aggregate__c FROM Account WHERE Id =: a.Id];
        //Verify that the Coverage Team Aggregate Field was Updated
        System.assert(a.Coverage_Team_Aggregate__c <> null);
        delete ctList;
        //Verify that the Coverage Team Aggregate Field was Updated/Removed
        a = [SELECT Coverage_Team_Aggregate__c FROM Account WHERE Id =: a.Id];
        System.assert(a.Coverage_Team_Aggregate__c == null);
    }
}