public class HL_OpportunityClientSubjectHandler {
    public static void UpdatePublicPrivate(List<Opportunity_Client_Subject__c> ocsList){
        //List of Opportunities to Update
        List<Opportunity__c> oppsToUpdate = new List<Opportunity__c>();
        //Set of Opp IDs
        Set<Id> opps = new Set<Id>();
        //Get a Map of Opps with Client/Subject ownership
        Map<Id,Set<String>> oppsToOwnership = new Map<Id,Set<String>>();
        Set<String> currentVals;
        for(Opportunity_Client_Subject__c ocs : ocsList)
            opps.Add(ocs.Opportunity__c);
        //Get all related Client/Subjects
        for(Opportunity_Client_Subject__c cs : [SELECT Opportunity__c, Client_Subject__r.Ownership FROM Opportunity_Client_Subject__c WHERE Opportunity__c IN:opps]){ 
            if(!oppsToOwnership.containsKey(cs.Opportunity__c))
                oppsToOwnership.put(cs.Opportunity__c, new Set<String> {cs.Client_Subject__r.Ownership});
            else{
                currentVals = oppsToOwnership.get(cs.Opportunity__c);
                currentVals.add(cs.Client_Subject__r.Ownership);
                oppsToOwnership.put(cs.Opportunity__c, currentVals);
            } 
        }
        //Update the Opportunities
        for(Opportunity__c opp : [SELECT Public_Or_Private__c FROM Opportunity__c WHERE Id IN:oppsToOwnership.keySet()]){
            currentVals = oppsToOwnership.get(opp.Id);
            if(currentVals.contains('Public Debt') || currentVals.contains('Public Equity'))
                opp.Public_Or_Private__c = 'Public';
            else
                opp.Public_Or_Private__c = 'Private';
            oppsToUpdate.add(opp);
        }
        
        if(oppsToUpdate.size() > 0)
            update oppsToUpdate;
    }
	
    public static void stopDeletion(List<Opportunity_Client_Subject__c> deletingRecordList) {
        for(Opportunity_Client_Subject__c  oppObj : deletingRecordList){
          if(oppObj.Primary__c ){  
             oppObj.addError('Primary Clients and Subjects cannot be deleted');
            }
        } 
    }
	
    public static void stopDuplicate(List<Opportunity_Client_Subject__c> InsertingACSRecords ) {
        set<ID> RelatedOpportunityIdSet = new set<ID>();
        set<ID> RelatedCompantyIdSet = new set<ID>();
        for(Opportunity_Client_Subject__c  oppObj1 : InsertingACSRecords){
          RelatedOpportunityIdSet.add(oppObj1.opportunity__c);
          RelatedCompantyIdSet.add(oppObj1.Client_Subject__c);
        }
        map<id, Account> RelatedAccountMap = new map<Id, Account> ([SELECT id, Name FROM Account WHERE id IN : RelatedCompantyIdSet]);
        map<id,set<string>> OpportunityWithRelatedExistingACSMap = new map<id,set<string>>();
        
        Set<String> CombinationSet;         
        for(opportunity__c opp : [SELECT ID,(SELECT id,Type__c,Client_Subject__c,Opportunity__c FROM Opportunity_Clients_Subjects__r) FROM Opportunity__c WHERE ID IN : RelatedOpportunityIdSet]) {
           CombinationSet = new Set<String>();
           for(Opportunity_Client_Subject__c acs : opp.Opportunity_Clients_Subjects__r) { 
              CombinationSet.add(acs.type__c+'_'+acs.Client_Subject__c);          
            }
           OpportunityWithRelatedExistingACSMap.put(opp.Id,CombinationSet);             
        }  
       
        for(Opportunity_Client_Subject__c ACSRecord : InsertingACSRecords){  
		             
            If(OpportunityWithRelatedExistingACSMap.containsKey(ACSRecord.Opportunity__c) && OpportunityWithRelatedExistingACSMap.get(ACSRecord.Opportunity__c) != null 
               && OpportunityWithRelatedExistingACSMap.get(ACSRecord.Opportunity__c).contains(ACSRecord.type__c+'_'+ACSRecord.Client_Subject__c) 
               && RelatedAccountMap.containsKey(ACSRecord.Client_Subject__c)) {
                ACSRecord.addError('Company Name : ' + '\'' +RelatedAccountMap.get(ACSRecord.Client_Subject__c).Name + '\'' + ' already exists as an Additional Client/Subject');             
            }
        }            
    }
         
    public static void validateDuplicateRecord(List<Opportunity_Client_Subject__c> OptyClientNewList, Map <Id,Opportunity_Client_Subject__c> optyClientOldMap) {
        Map<String, Opportunity_Client_Subject__c> OptyClientUniqueMap = new Map<String, Opportunity_Client_Subject__c>();
        set<Id> OpportunityIdSet = new Set<Id>();
        set<Opportunity_Client_Subject__c> BulkyLoaderSet = new Set<Opportunity_Client_Subject__c>();
        for (Opportunity_Client_Subject__c OptyClient : OptyClientNewList){             
            //Check for duplicate Primary Address in same transaction
            if (((OptyClient.Client_Subject__c+''+OptyClient.Opportunity__c+''+OptyClient.Type__c) !=null) && 
                ((OptyClient.Client_Subject__c+''+OptyClient.Opportunity__c+''+OptyClient.Type__c) != 
                    (optyClientOldMap.get(OptyClient.Id).Client_Subject__c+''+optyClientOldMap.get(OptyClient.Id).Opportunity__c+''+optyClientOldMap.get(OptyClient.Id).Type__c))) {
                    
               		BulkyLoaderSet.add(OptyClient);
                    // Check for existing Primary Address       
                    if (OptyClientUniqueMap.containsKey(OptyClient.Client_Subject__c+''+OptyClient.Opportunity__c+''+OptyClient.Type__c)){
                       OptyClient.addError('Company Name : ' + '\'' + OptyClient.Client_Subject__r.Name + '\''+ 'with this Type already exists');
                    }else{
                       OptyClientUniqueMap.put((OptyClient.Client_Subject__c+''+OptyClient.Opportunity__c+''+OptyClient.Type__c), OptyClient);
                       OpportunityIdSet.add(OptyClient.Opportunity__c);
                    }
            }
            
        }
        
        for (Opportunity_Client_Subject__c optyClientRecord  : [SELECT Client_Subject__c, Opportunity__c,Type__c,Client_Subject__r.Name
                                                                FROM Opportunity_Client_Subject__c
                                                                WHERE Opportunity__c IN :OpportunityIdSet AND ID NOT IN : BulkyLoaderSet limit : Limits.getLimitQueryRows()]){
            if(OptyClientUniqueMap.containsKey(optyClientRecord.Client_Subject__c+''+optyClientRecord.Opportunity__c+''+optyClientRecord.Type__c)) {
                Opportunity_Client_Subject__c duplicateRecord = OptyClientUniqueMap.get(optyClientRecord.Client_Subject__c+''+optyClientRecord.Opportunity__c+''+optyClientRecord.Type__c);
                duplicateRecord.addError('Company Name : ' + '\'' +optyClientRecord.Client_Subject__r.Name + '\''+ 'with this Type already exists');
            }
        }                                
    }
}