public with sharing  virtual class HL_TimeRecordControllerBase {
    public HL_TimeRecordControllerBase() {}
 
    //Determines if the user is in the supervisor group
    public static Boolean IsSupervisor(string category){
        return HL_Group.IsInGroup('Time_Tracking_' + category + '_Supervisor');
    }

    //Determines if the user is in the non-supervisor group
    public static Boolean IsInEntryGroup(string category){
        return HL_Group.IsInGroup('Time_Tracking_' + category);
    }

    public static Time_Record_Period__c GetCurrentPeriod(string category){
        return HL_TimeRecordPeriod.GetCurrentPeriod(category);
    }

    public static Time_Record_Period_Staff_Member__c GetCurrentTimeRecordPeriodStaffMemberRecord(string category){
        Contact c = HL_Contact.GetByUserId();
        Time_Record_Period__c trp = GetCurrentPeriod(category);

        HL_TimeRecordPeriodStaffMember sm = new HL_TimeRecordPeriodStaffMember(trp, c);
        return category == 'FR' ? sm.GetRecord(false) : sm.GetRecord(IsInEntryGroup(category));
    }

    public static Time_Record_Period_Staff_Member__c GetTimeRecordPeriodStaffMemberRecord(string category, Time_Record_Period__c timePeriod){
        Contact c = HL_Contact.GetByUserId();

        HL_TimeRecordPeriodStaffMember sm = new HL_TimeRecordPeriodStaffMember(timePeriod, c);
        return category == 'FR' ? sm.GetRecord(false) : sm.GetRecord(IsInEntryGroup(category));
    }

    public static List<Opportunity__c> GetOpportunities(string category, string userId){
        List<Opportunity__c> results = category == 'FR' ? HL_Opportunity.GetByUser(userId, new Set<String>{'Active','Engaged', 'Hold'} , Date.today().addMonths(-2)) :
                                                          HL_Opportunity.GetByUser(userId, new Set<String>{'Active', 'Hold'} , Date.today());
        /*
        List<Opportunity__c> filteredResults = new List<Opportunity__c>();

        if (category == 'Litigation' || category == 'TFR' || category == 'beta') {
            for (Opportunity__c opp: results) {
                if (category == 'Litigation' && opp.Job_Type__c.startsWith('Lit')) {
                    filteredResults.add(opp);
                }
                else if (category == 'TFR' && opp.Job_Type__c.startsWith('TFR')) {
                    filteredResults.add(opp);
                }
                else if (category == 'beta' && opp.Job_Type__c.startsWith('TAS')) {
                    filteredResults.add(opp);
                }
            }
            return filteredResults;
        }
        else {
            */
            return results;
        //}  
    }

    public static List<Engagement__c> GetEngagements(string category, string userId){

        List<Engagement__c> results = category == 'FR' ? HL_Engagement.GetByUser(userId, new Set<String>{'Active','Closed','Hold'}, Date.today().addMonths(-2)) :
                                                         HL_Engagement.GetByUser(userId, new Set<String>{'Active','Closed','Hold'}, Date.today());

        /*List<Engagement__c> filteredResults = new List<Engagement__c>();

        if (category == 'Litigation' || category == 'TFR' || category == 'beta') {
            for (Engagement__c eng: results) {
                if (category == 'Litigation' && eng.Job_Type__c.startsWith('Lit')) {
                    filteredResults.add(eng);
                }
                else if (category == 'TFR' && eng.Job_Type__c.startsWith('TFR')) {
                    filteredResults.add(eng);
                }
                else if (category == 'beta' && eng.Job_Type__c.startsWith('TAS')) {
                    filteredResults.add(eng);
                }
            }
            return filteredResults;
        }
        else {
		*/
            return results;
        
    	//}  
    }

    public static List<Special_Project__c> GetSpecialProjects(Id recordTypeId){
        return HL_SpecialProject.GetByRecordTypeId(recordTypeId);
    }
}