public with sharing class HL_MonthlyRevenueProcessControlHandler{
	public static boolean isTest = false;
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public HL_MonthlyRevenueProcessControlHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }

    public void onBeforeUpdate(List<Monthly_Revenue_Process_Control__c> mrpcList, Map<id,Monthly_Revenue_Process_Control__c> mrpcOldMap){
        List<Monthly_Revenue_Process_Control__c> mrpcUnlockList = new List<Monthly_Revenue_Process_Control__c>();

        for(Monthly_Revenue_Process_Control__c mrpc : mrpcList){
            Monthly_Revenue_Process_Control__c mrpcOld = mrpcOldMap.get(mrpc.id);

        	//If the record is current and staff summary report sent is completed, we want to unlock 
        	if(mrpc.IsCurrent__c && mrpc.Staff_Summary_Report_Sent__c && mrpc.Revenue_Accruals_Locked__c)
        		mrpcUnlockList.add(mrpc);

            //We only want one record to have the isCurrent flag marked.
            if(mrpc.IsCurrent__c <> mrpcOld.IsCurrent__c && mrpc.IsCurrent__c == true && [SELECT ID FROM Monthly_Revenue_Process_Control__c WHERE IsCurrent__c = true LIMIT 1].size() > 0)
                mrpc.addError('There is already a record that has the "is Current" field checked, you can only indicated one record as current.  Please go back and uncheck the "is Current" flag on the existing record before marking another record as current.');
            }

        if(mrpcUnlockList.size() > 0){
        	//Unlock should happen first so that way we don't have a current record
        	Unlock(mrpcUnlockList);

        	ClearEngagementCurrentRevenueAccrual(mrpcList);
        }
    }

    private void Unlock(List<Monthly_Revenue_Process_Control__c> mrpcList){
        //Unlock the General Setting
        HL_General__c hg = HL_General__c.getOrgDefaults();
        hg.Revenue_Accrual_Locked__c = false;
        update hg;

        for(Monthly_Revenue_Process_Control__c mrpc : mrpcList){
        	mrpc.IsCurrent__c = false;
        	mrpc.Revenue_Accruals_Locked__c = false;
        }
        CreateNextMonthlyRevenueProcessControl(mrpcList);
    }

    private void ClearEngagementCurrentRevenueAccrual(List<Monthly_Revenue_Process_Control__c> mrpcList){
    	Map<Id, Id> engAccrualMap = new Map<Id, Id>();
    	List<Engagement__c> engUpdateList = new List<Engagement__c>();

    	//Get Engagements to Clear
    	for(Revenue_Accrual__c ra : [SELECT Engagement__c, Engagement__r.Current_Revenue_Accrual__c FROM Revenue_Accrual__c WHERE Monthly_Revenue_Process_Control__c IN: mrpcList AND Engagement__r.Current_Revenue_Accrual__c <> null])
    		engAccrualMap.put(ra.Engagement__c, ra.Engagement__r.Current_Revenue_Accrual__c);

    	for(Id id : engAccrualMap.keySet())
    		engUpdateList.add(new Engagement__c(Id = id, Latest_Revenue_Accrual__c = engAccrualMap.get(id), Current_Revenue_Accrual__c = null, Period_Accrued_Fees__c = null));

    	if(engUpdateList.size() > 0){
            //Block the Engagement Trigger from Firing
            HL_TriggerContextUtility.ByPassOnMonthlyRevenueProcess = true;

            //Perform Updates
    		update engUpdateList;

            //Unblock Engagement Trigger from Firing
            HL_TriggerContextUtility.ByPassOnMonthlyRevenueProcess = false;
        }
    }

     private void CreateNextMonthlyRevenueProcessControl(List<Monthly_Revenue_Process_Control__c> processControlList){
        List<Monthly_Revenue_Process_Control__c> nextProcessControlList = new List<Monthly_Revenue_Process_Control__c>();
        Set<Date> holidaySet = new Set<Date>();
        for(Holiday holidays : [SELECT ActivityDate FROM Holiday]){
            holidaySet.add(holidays.ActivityDate);
        }

        for (Monthly_Revenue_Process_Control__c previousProcessControl : processControlList){
            Monthly_Revenue_Process_Control__c nextProcessControl = new Monthly_Revenue_Process_Control__c();
            nextProcessControl.IsCurrent__c = true;
            Integer previousMonth = integer.valueof(previousProcessControl.Current_Month__c);
            Integer previousYear = integer.valueof(previousProcessControl.Current_Year__c);
            Integer currentMonth = previousMonth == 12 ? 1 : previousMonth + 1;
            Integer currentYear = previousMonth == 12 ? previousYear + 1 : previousYear;
            nextProcessControl.Current_Month__c = string.valueof(currentMonth).leftPad(2).replace(' ', '0');
            nextProcessControl.Current_Year__c = string.valueof(currentYear);
            DateTime monthBeginDate = DateTime.newInstance(currentYear, currentMonth, 1);
            nextProcessControl.Name = monthBeginDate.format('MMMMM') + ' ' + nextProcessControl.Current_Year__c;
            DateTime lastDayOfMonth = monthBeginDate.addDays(Date.daysInMonth(currentYear, currentMonth)-1);
            nextProcessControl.Revenue_Date__c = date.valueof(lastDayOfMonth);
            DateTime nextMonth = lastDayOfMonth.addDays(1);
            DateTime finalChanges;
            
            if([SELECT Id FROM Monthly_Revenue_Process_Control__c WHERE Current_Month__c =: nextProcessControl.Current_Month__c AND Current_Year__c =: nextProcessControl.Current_Year__c LIMIT 1].size() == 0){

                if(!(lastDayOfMonth.format('EEEE') == 'Saturday' || lastDayOfMonth.format('EEEE') == 'Sunday' || holidaySet.contains(lastDayOfMonth.date()))){
                    lastDayOfMonth = lastDayOfMonth.addDays(-1);
                }
                else {
                    while(lastDayOfMonth.format('EEEE') == 'Saturday' || lastDayOfMonth.format('EEEE') == 'Sunday' || holidaySet.contains(lastDayOfMonth.date())){
                    lastDayOfMonth = lastDayOfMonth.addDays(-1);
                }
            }

            finalChanges = lastDayOfMonth;

                while(finalChanges.format('EEEE') == 'Saturday' || finalChanges.format('EEEE') == 'Sunday' || holidaySet.contains(finalChanges.date())){
                    finalChanges = finalChanges.addDays(-1);
                }

                nextProcessControl.Final_Changes_Due__c = date.valueof(finalChanges);
                
                while(nextMonth.format('EEEE') == 'Saturday' || nextMonth.format('EEEE') == 'Sunday' || holidaySet.contains(nextMonth.date())){
                    nextMonth = nextMonth.addDays(1);
                }
                
                nextProcessControl.Final_Report_Date__c = date.valueof(nextMonth);
                nextProcessControlList.add(nextProcessControl);
                insert nextProcessControlList;
            }
        }
    }
}