@isTest (SeeAllData=false)
private class Test_HL_OpportunityViewController {
   private static User createTestUser(Id profID, String fName, String lName)
   {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId
                             );
        return tuser;
    }
    
    Static List<Delegate_Public_Group__c> dpgList;
    Static List<Opportunity_Internal_Team__c> oitList;
    Static List<Contact> conList ;
    Static  List<User> usr;
    @testSetup
    private static void setup() {    
        usr = new List<User>();        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User user1 = Test_HL_OpportunityViewController.createTestUser(pf.Id, 'Test FirstName', 'Test LastName');        
        usr.add(user1);
        insert usr;
                 
        List<Account> accList = (List<Account>)HL_TestFactory.CreateSObjectList('Account', false, 1);        
        for(integer i = 0; i < accList.size(); i++){
            accList[i].Name = 'Test_' + String.valueOf(i);        
            accList[i].BillingCountry = 'United States';
            accList[i].BillingState = 'California';
            accList[i].ShippingCountry = accList[i].BillingCountry;
            accList[i].ShippingState = accList[i].BillingState;      
        }
        insert accList;
        
        conList = SL_TestSetupUtils.CreateContact('Contact', 1, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        for(integer i = 0; i < conList.size(); i++){
            conList[i].FirstName = 'DND Test';
            conList[i].LastName = 'Test LN';
            conList[i].AccountId = accList[0].Id;            
            conList[i].User__c = user1.Id;
        }
        insert conList;
        
        Opportunity__c opportunity =(Opportunity__c)HL_TestFactory.CreateSObject('Opportunity__c',false);        
        opportunity.Name='Test Opportunity';
        opportunity.Client__c = accList[0].Id;
        opportunity.Subject__c = accList[0].Id;
        opportunity.Stage__c = 'Pitched';
        opportunity.Line_of_Business__c = 'CF';
        opportunity.Job_Type__c = 'Financing';
        opportunity.Primary_Office__c = 'AT';
        insert opportunity;
        
        List<String> oppTeamRoles = new List<String> {'Principal','Seller','Manager','Associate','Initiator','Analyst'};
        List<Staff_Role__c> staffRoles = new List<Staff_Role__c>();
        
        for(Integer i = 0; i<oppTeamRoles.size(); i++)
            staffRoles.add(new Staff_Role__c(Name=oppTeamRoles[i], Display_Name__c = oppTeamRoles[i], CF__c = true, FAS__c = true));
        insert staffRoles;
        
        oitList = (List<Opportunity_Internal_Team__c>)HL_TestFactory.CreateSObjectList('Opportunity_Internal_Team__c', false, 1);
        for(integer i = 0; i < oitList.size(); i++){
            oitList[i].Contact__c = conList[i].Id;
            oitList[i].Opportunity__c = opportunity.Id;
            oitList[i].Staff_Role__c = staffRoles[0].id;
            oitList[i].Start_Date__c = Date.today();
           
        }
        insert oitList; 
        
        dpgList = (List<Delegate_Public_Group__c>)HL_TestFactory.CreateSObjectList('Delegate_Public_Group__c', false, 1);
        for(integer i = 0; i < dpgList.size(); i++){
            dpgList[i].Banker__c = user1.id;
        }
        insert dpgList; 
        
        Delegate_Public_Group_Member__c DPGmember1 = new Delegate_Public_Group_Member__c();
        DPGmember1.Delegate__c = user1.id;
        DPGmember1.Delegate_Public_Group__c = dpgList[0].id;        
        list<Delegate_Public_Group_Member__c> objDPGMemberlist = new list<Delegate_Public_Group_Member__c>(); 
        objDPGMemberlist.add(DPGmember1);             
        insert objDPGMemberlist;        
    }
    @isTest 
    public static void TestMethforSystemAdminFunctionality(){
        test.startTest();   
        Opportunity__c opp = [SELECT Id FROM Opportunity__c LIMIT 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        HL_OpportunityViewController con = new HL_OpportunityViewController(sc);
        Boolean onOIT = con.OnOIT;
        test.stopTest();
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));        
    }
    @isTest
    public static void testRunAsNonSysAdm() {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'CAO' LIMIT 1]; 
        User u = new User(Alias = 'tooiyr', Email='standardtestusersfdc@force.com', 
                    EmailEncodingKey='UTF-8', LastName='TestUserQADEV', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = p.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='standardtestuser@force.com');  
        insert u;       
             
        Account accObj = [SELECT Id, OwnerId, Name FROM Account LIMIT 1];
        accObj.OwnerId=u.id;
        update accObj;     
        Contact conObj =[SELECT Id,Name,OwnerId,User__c FROM Contact LIMIT 1];
        conObj.OwnerId=u.Id;
        conObj.User__c =u.Id;
        update conObj;
        Opportunity__c opp  = [SELECT Id, Name, OwnerId FROM Opportunity__c];  
        opp.OwnerId= u.id;
        update opp;        
        Opportunity_Internal_Team__c  oitObj =[SELECT Id, OwnerId, Name FROM Opportunity_Internal_Team__c LIMIT 1];
        oitObj.OwnerId =u.Id;
        update oitObj;
        System.runAs(u) {   
            test.startTest(); 
            Delegate_Public_Group__c dpg = new Delegate_Public_Group__c();
            dpg.Banker__c = u.Id;
            insert dpg;            
            Delegate_Public_Group_Member__c dpmg = new Delegate_Public_Group_Member__c();
            dpmg.Delegate__c=u.id;
            dpmg.Delegate_Public_Group__c =dpg.Id;
            Insert dpmg;
  
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            HL_OpportunityViewController con = new HL_OpportunityViewController(sc);            
            test.stopTest();
            System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));
        }
    }
}