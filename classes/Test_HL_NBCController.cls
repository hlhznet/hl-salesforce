@isTest
private class Test_HL_NBCController {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Account a = SL_TestSetupUtils.CreateAccount('',1)[0];
        insert a;
        Opportunity__c o = SL_TestSetupUtils.CreateOpp('', 1)[0];
        insert o;
        Opportunity_Approval__c oa = SL_TestSetupUtils.CreateOA('', 1)[0];
        oa.Related_Opportunity__c = o.Id;
        insert oa;
        Test.startTest();
        	//Setup Standard Controller
            ApexPages.StandardController sc = new ApexPages.StandardController(oa);
            //Test Controller Properties/Methods
            HL_NBCController con = new HL_NBCController(sc);
            List<Financials__c> financials = con.Financials;
            Financials__c financial = con.NewFinancials;
            con.NewFinancials.Related_Account__c = a.Id;
            con.RefreshFinancials();
            con.SaveAndReturn();
            con.SaveNewFinancial();
            con.SaveRecord();
        Test.stopTest();
        //Confirm there are no errors
        System.assert(!ApexPages.hasMessages(ApexPages.severity.ERROR));
    }
}