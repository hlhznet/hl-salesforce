public without sharing class HL_EngVPPositionNewController {
    
    Public Eng_VP_Position__c EngVPPositon {get; set;}
    public Boolean isValidPVUser{get;set;}
    public Boolean IshavingValidRoleForRevAccrual{get;set;}
    public HL_EngVPPositionNewController(ApexPages.StandardController controller) {        
        EngVPPositon = new Eng_VP_Position__c();
        EngVPPositon = (Eng_VP_Position__c) controller.getRecord(); 
        EngVPPositon.Engagement_VP__c = apexpages.currentPage().getParameters().get('VPId');   
        EngVPPositon.Status__c = 'In Progress';
        
        Engagement__c Eng = new Engagement__c();
        for(Eng_VP__c engVP : [Select Engagement__c From Eng_VP__c Where Id =: EngVPPositon.Engagement_VP__c])
        {
            Eng.id = engVP.Engagement__c;
        }
        if(Eng != null && Eng.id != null )
            isValidPVUser = HL_EngagementViewController.VerifyUserforVP(Eng, 'PVPositionCreation');  
            IshavingValidRoleForRevAccrual = HL_ConstantsUtil.IshavingValidRoleInInternTeamEng(Eng.id);                    
    }
    
    public PageReference SaveData() {
        
        try{ 
            insert EngVPPositon; 
            return new pageReference('/'+ apexpages.currentPage().getParameters().get('VPId'));
        }
        catch(Exception e) {
            Apexpages.addMessages(e);
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()));
            return null;
        }       
        
    }
}