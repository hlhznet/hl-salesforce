Public Class HL_ConstantsUtil {

    Public Static Boolean StopValidation = false; 
    Public Static Boolean AllowExecutionForManualInsertEngCounterparty = true;
    // Purpose is to stop populating company Name in to VP Position Name while importing
    Public Static Boolean stopCopyingNameFromCompToPosition = false; 
    Public Static Boolean stopExecutionForPVConversion = false; 
    Public Static Boolean engagementValue = false;
    Public Static list<Engagement_Internal_Team__c> EngInternalTeamMemberList = new list<Engagement_Internal_Team__c>();
    Public Static List<Group> PVGroupList = new List<Group>();
    Public Static List<Delegate_Public_Group__c> DPGroupList = new List<Delegate_Public_Group__c>();
    Public Static Boolean stopExecuteEstFeeUpdateTrigger = false;
    Public Static Boolean ToStopChangesStatusToCancelled = true;
    Public Static Boolean ToStopChangesCurrency = true;
    Public Static Boolean stopUpdatingPVValueStatusChange = false;
    Public Static Boolean resultFromSupervisorCalculation = false;
    Public Static Boolean isSupervisorMethodExecuted = false;
    //Public Static Boolean stopValidationFromInactiveContact = false;
            
    public static Boolean IsSupervisorOrAdmin(){
        boolean isSupervisorOrAdmin = false;              
        isSupervisorOrAdmin  = HL_OpportunityViewController.IsUserMemberofPVSuperVisorOrItschildGroup();           
        if(!isSupervisorOrAdmin)
        for(User u : [SELECT Id from User Where Id =: userinfo.getUserId() AND Profile.Name = 'System Administrator'])
        {
            isSupervisorOrAdmin = true;            
        }        
        return isSupervisorOrAdmin;
    }    
       
    public static Boolean IsHavingPVPermissionSetOrIsSysAdmin (){
        boolean IsHavingPVPermissionSetOrisSysAdmin = false;                                      
        for(User u : [SELECT Id from User Where Id =: userinfo.getUserId() AND Profile.Name = 'System Administrator'])
        {
            IsHavingPVPermissionSetOrisSysAdmin = true;            
        } 
        if(!IsHavingPVPermissionSetOrisSysAdmin ){
            for(PermissionSetAssignment  PSA : [SELECT Id from PermissionSetAssignment Where AssigneeId =: UserInfo.getUserId() AND PermissionSet.Name =: Label.HL_PVPermissionSetName])
            {
                IsHavingPVPermissionSetOrisSysAdmin = true;            
            }       
        } 
        return IsHavingPVPermissionSetOrisSysAdmin ;
    }    
    
    // Method to identify if logged in user's role with respect to deal team on Engagement
    public static Boolean IshavingValidRoleInInternTeamEng(Id EngagementId){
        boolean isHavingValidRoleOrSupervisor = false;
        list<String> RoleList = new list<String>();
        RoleList = system.label.HL_ValidRolesForRevenueAccrual.split(';');
        Set<Id> SetofValidDealTeamMembers = new Set<Id>();
        for (Engagement_Internal_Team__c eit : [SELECT Id,Contact__r.User__c FROM Engagement_Internal_Team__c where End_Date__c = null AND Engagement__c =: EngagementId AND Role__c IN : RoleList ]){
            SetofValidDealTeamMembers.add(eit.Contact__r.User__c);            
        }  
        Set<Id> SetOFMasterUserIds = new Set<Id>();
        for(Delegate_Public_Group_Member__c DGM : [Select id , Delegate_Public_Group__r.Banker__c From Delegate_Public_Group_Member__c Where Delegate__c =: userInfo.getUserId()])
        {
            if(DGM.Delegate_Public_Group__r.Banker__c != null)
                SetOFMasterUserIds.add(DGM.Delegate_Public_Group__r.Banker__c);
        }
        SetOFMasterUserIds.add(userInfo.getUserId());
        for(ID containuser : SetOFMasterUserIds){        
            if(SetofValidDealTeamMembers.contains(containuser))
            {
                isHavingValidRoleOrSupervisor = true;
                break;
            }            
        }        
        if(!isHavingValidRoleOrSupervisor)
            isHavingValidRoleOrSupervisor = IsSupervisorOrAdmin();
        if(!isHavingValidRoleOrSupervisor)
        {
            for(User u : [SELECT Id, Profile.Name from User Where Id =: userinfo.getUserId() AND Profile.Name = 'System Administrator (Read Only)'])
                isHavingValidRoleOrSupervisor = true;
        }
        return isHavingValidRoleOrSupervisor;
    }
    
    // Method to identify if logged in user's role with respect to deal team on Opportunity
    public static Boolean IshavingValidRoleInInternTeamOpp(Id opportunityId){
        boolean isHavingValidRoleOrSupervisor = false;
        list<String> RoleList = new list<String>();
        RoleList = system.label.HL_ValidRolesForRevenueAccrual.split(';');
        Set<Id> SetofValidDealTeamMembers = new Set<Id>();        
        for (Opportunity_Internal_Team__c eit : [SELECT Id,Contact__r.User__c FROM Opportunity_Internal_Team__c where End_Date__c = null AND Opportunity__c =: opportunityId AND Role__c IN : RoleList]){
            SetofValidDealTeamMembers.add(eit.Contact__r.User__c);            
        }  
        Set<Id> SetOFMasterUserIds = new Set<Id>();        
        for(Delegate_Public_Group_Member__c DGM : [Select id , Delegate_Public_Group__r.Banker__c From Delegate_Public_Group_Member__c Where Delegate__c =: userInfo.getUserId()])
        {
            if(DGM.Delegate_Public_Group__r.Banker__c != null)
                SetOFMasterUserIds.add(DGM.Delegate_Public_Group__r.Banker__c);               
        }
        SetOFMasterUserIds.add(userInfo.getUserId());        
        for(ID containuser : SetOFMasterUserIds){        
            if(SetofValidDealTeamMembers.contains(containuser))
            {
                isHavingValidRoleOrSupervisor = true;
                break;
            }
        }     
        if(!isHavingValidRoleOrSupervisor)
            isHavingValidRoleOrSupervisor = IsSupervisorOrAdmin();
        if(!isHavingValidRoleOrSupervisor)
        {
            for(User u : [SELECT Id, Profile.Name from User Where Id =: userinfo.getUserId() AND Profile.Name = 'System Administrator (Read Only)'])
                isHavingValidRoleOrSupervisor = true;
        }
        return isHavingValidRoleOrSupervisor;
    }   
}