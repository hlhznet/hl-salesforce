public without sharing class HL_EngValuationPeriodPositionController {
   
    public String userProfileName {
        get {
            return [
                    select Profile.Name
                    from User
                    where Id = :Userinfo.getUserId() 
                    ].Profile.Name;
        }
    }

    public List<VPTeamMember> VPTeamMemberList {get;set;}
    public Integer selectItemSrNo {get;set;}
    private Id CurrentPositionId;
    public Boolean isValidPosition {get; private set;}
    public Boolean isChangesSave {get;set;}
    public Boolean isErrorOnSave {get;set;}
    public Integer NoOfAddedTeamMemberButNotSaved {get; set;}
    public Set<id> AddedTeamMemberIdSet {get; set;}
    public  Map<Id, Contact> ContactMap;
    public Integer noOfHLEmployees {get;set;}
    public Boolean IsSaveRequired {get; set;}
    public Boolean UserCanCreateModifyTM {get; set;}        
    public Boolean UserCanDeleteTM {get; set;}
    private Set<Id> tempVPTMSet; // Set of Ids for team members added temporarily   
    public boolean isPositionCancelled {get; set;}
    public boolean IshavingValidRoleForRevAccrual {get; set;}     
    public Boolean isValidPVUser{get;set;}
    private Id EngVpId;
    private Map<Id,String> OverallContactMap;
    Public Eng_VP_Position__c  OriginalPosition;         
    public String fieldAPI{get;set;}  
    Public Eng_VP_Position__c EngVPPosition{get;set;}
    Public boolean isError{get;set;}
    Public Boolean IsHavingPVPermissionSetOrIsSysAdmin{get;set;} 
    private string tempName;        
    public Integer AllowedMembers {get 
        {
            ContactMap = new Map<Id, Contact>();
            Integer newContactsTobeadded = 0;
            noOfHLEmployees = 0;
            Set<Id> ContactlinkedToTeamMemberSet = new Set<Id>();
            for(Eng_VP_TeamMember__c opVPtm : [SELECT staff__c, Is_Team_Member_Deactivated_Once__c  FROM Eng_VP_TeamMember__c WHERE Is_Team_Member_Deactivated_Once__c = false AND Engagement_VP_Position__c =: CurrentPositionId]) {
                ContactlinkedToTeamMemberSet.add(opVPtm.staff__c);
            }            
            for(Contact c : [SELECT id, Name from Contact WHERE Id IN (SELECT Contact__c FROM Engagement_Internal_Team__c WHERE End_Date__c = null AND Engagement__c = : EngagementId)]) {
              ContactMap.put(c.id, c);
              OverallContactMap.put(c.id,c.Name);
              noOfHLEmployees ++;
              if (!ContactlinkedToTeamMemberSet.contains(c.Id))
                newContactsTobeadded++;               
            }
            AllowedMembers = newContactsTobeadded; //ContactMap.values().size();
            Set<Id> InactiveMembers = new Set<Id>();
            Set<Id> activeMembers = new Set<Id>();
            for(Eng_VP_TeamMember__c EngVPtm : [SELECT staff__c, Team_Member_Status__c  FROM Eng_VP_TeamMember__c WHERE Is_Team_Member_Deactivated_Once__c = false AND staff__c IN: ContactMap.KeySet()  AND Engagement_VP_Position__c =: CurrentPositionId ORDER BY CREATEDDATE DESC])
            {
                InactiveMembers.add(EngVPtm.staff__c);
            }
            system.debug('--InactiveMembers--'+InactiveMembers.size());   
            system.debug('--AllowedMembers--'+AllowedMembers); 
            InactiveMembers.addAll(tempVPTMSet);     
            AllowedMembers += InactiveMembers.size();
            system.debug('--AllowedMembers--'+AllowedMembers);
            return AllowedMembers;
        } set;
    }
        
    private void modifySet() {
            Set<Id> InactiveMembers = new Set<Id>();
            Set<Id> activeMembers = new Set<Id>();
            for(Eng_VP_TeamMember__c EngVPtm : [SELECT staff__c,Team_Member_Status__c  FROM Eng_VP_TeamMember__c WHERE Is_Team_Member_Deactivated_Once__c = false AND Engagement_VP_Position__c =: CurrentPositionId ORDER BY CREATEDDATE DESC])
            {
                InactiveMembers.add(EngVPtm.staff__c);
            }            
            InactiveMembers.addAll(tempVPTMSet);    
            AddedTeamMemberIdSet.removeAll(InactiveMembers);                                   
   }
        
    public List<SelectOption> HLInternalEmployees {get
        {
            HLInternalEmployees = new List<SelectOption>();
            modifySet();// Added this method for SF-821
            for(Contact cont : [SELECT id, Name from Contact WHERE Id IN (SELECT Contact__c FROM Engagement_Internal_Team__c WHERE End_Date__c = null AND Engagement__c = : EngagementId) AND Id NOT IN : AddedTeamMemberIdSet ORDER BY Name ASC]) {
              HLInternalEmployees.add(new SelectOption(cont.Id, Cont.Name));
            }
            return HLInternalEmployees;
        }
        set;
    }
        
    private Id EngagementId;
    public string parentId {
        get{
            return EngagementId;
            } set;
    }
       
    //Refreshes the list of possible role choices for a new staff member  
    public void RefreshNewStaffRoles() {
        //StaffMemberToAdd = null;
    }

    public HL_EngValuationPeriodPositionController(ApexPages.StandardController controller){
        AllowedMembers = 0;
        isPositionCancelled  = false;
        tempVPTMSet = new Set<Id>();
        IsSaveRequired = false;
        ContactMap = new Map<Id, Contact>();
        OverallContactMap = new Map<Id, String>();
        HLInternalEmployees = new List<SelectOption>();
        NoOfAddedTeamMemberButNotSaved = 0;
        EngVPPosition = new Eng_VP_Position__c();
        EngVPPosition = (Eng_VP_Position__c)controller.getRecord();
        CurrentPositionId =  apexpages.currentPage().getParameters().get('id');
        AddedTeamMemberIdSet = new Set<id>();
        tempName = '';        
        FetchLatestTeamMembers();
        List<Contact> ValidContactList = new List<Contact>([SELECT id, Name from Contact WHERE Id IN (SELECT Contact__c FROM Engagement_Internal_Team__c WHERE End_Date__c = null AND Engagement__c = : EngagementId)]);
            AllowedMembers = ValidContactList.size();
        isValidPVUser = HL_ConstantsUtil.IsSupervisorOrAdmin();
        IshavingValidRoleForRevAccrual = HL_ConstantsUtil.IshavingValidRoleInInternTeamEng(EngagementId);   
        IsHavingPVPermissionSetOrIsSysAdmin = HL_ConstantsUtil.IsHavingPVPermissionSetOrIsSysAdmin();        
        VerifyAccessibilityToTM();
        OriginalPosition = new Eng_VP_Position__c();
        for(Eng_VP_Position__c vpPos1 : [SELECT Name, Asset_Classes__c, Notes__c, Report_Fee__c, Engagement_VP__c,Company__c, Engagement_VP__r.Engagement__c,Status__c,Revenue_Month__c, Revenue_Year__c FROM Eng_VP_Position__c WHERE id =: CurrentPositionId ]) 
        {   
            OriginalPosition = vpPos1;
        }        
    }
    
    private void FetchLatestTeamMembers() {
        tempVPTMSet = new Set<id>();
        VPTeamMemberList = new List<VPTeamMember>();    
        Monthly_Revenue_Process_Control__c  MRPC = new Monthly_Revenue_Process_Control__c();
            MRPC = HL_Eng_VP_PositionTriggerHelper.fetchMRPC();
        for(Eng_VP_Position__c vpPos : [SELECT Name, Asset_Classes__c, Notes__c, Report_Fee__c, Engagement_VP__c,Company__c, Engagement_VP__r.Engagement__c,Status__c,Revenue_Month__c, Revenue_Year__c,   (SELECT id, staff__c, staff__r.Name, Role__c, Start_Date__c, End_Date__c, Team_Member_Status__c, Engagement_VP_Position__c, Is_Team_Member_Deactivated_Once__c FROM Eng_VP_TeamMembers__r ORDER BY  Team_Member_Status__c DESC,staff__r.Name ASC   ) FROM Eng_VP_Position__c WHERE id =: CurrentPositionId ]) 
        {
            EngVPPosition = vpPos;
            if(vpPos.Status__c =='Cancelled')
              isPositionCancelled = true;            
            if(MRPC != null && vpPos.Status__c == System.Label.HL_Eng_VP_Status_Completed && HL_Eng_VP_PositionTriggerHelper.isRevenueMonthYearPassed(vpPos.Revenue_Month__c,vpPos.Revenue_Year__c, MRPC ))
              isPositionCancelled = true;
            isValidPosition = true;
            EngagementId = vpPos.Engagement_VP__r.Engagement__c;
            for(Eng_VP_TeamMember__c vptm : vpPos.Eng_VP_TeamMembers__r)
            {
                VPTeamMemberList.add(new VPTeamMember(VPTeamMemberList.size()+1, vptm));
                if(!vptm.Is_Team_Member_Deactivated_Once__c) {
                    AddedTeamMemberIdSet.add(vptm.staff__c);
                    IsSaveRequired = true;
                }
                OverallContactMap.put(vptm.staff__c, vptm.staff__r.Name);
           }
           EngVpId = vpPos.Engagement_VP__c;
        }
        
        for(Contact c: [SELECT id, Name from Contact WHERE Id IN (SELECT Contact__c FROM Engagement_Internal_Team__c WHERE End_Date__c = null AND Engagement__c = : EngagementId)])
        {
            OverallContactMap.put(c.id, c.Name);
        }
        NoOfAddedTeamMemberButNotSaved = 0;
    }   
       
    public PageReference AddMember() {
        IsSaveRequired = true;
        VPTeamMemberList.add(new VPTeamMember(VPTeamMemberList.size()+1 , new Eng_VP_TeamMember__c(Role__c = system.Label.HL_DefaultTeamMember, Engagement_VP_Position__c = CurrentPositionId, Start_Date__c = system.Today())));
        NoOfAddedTeamMemberButNotSaved++;
        for (VPTeamMember vptm : VPTeamMemberList) {            
          if(vptm.EngVPTeamMember.Id == null || (vptm.EngVPTeamMember.Id != null && !vptm.EngVPTeamMember.Is_Team_Member_Deactivated_Once__c)){
            AddedTeamMemberIdSet.add(vptm.SelectedStaffId);
            if(vptm.EngVPTeamMember.Id == null)
                tempVPTMSet.add(vptm.SelectedStaffId);
          }
          if(OverallContactMap.containsKey(vptm.SelectedStaffId))
            vptm.SelectedStaffName = OverallContactMap.get(vptm.SelectedStaffId); 
        }
        return null;
    }
       
    public PageReference RemoveMember() {
        isChangesSave = false;
        IsSaveRequired =  false;
        NoOfAddedTeamMemberButNotSaved--;        
        for (VPTeamMember vptm : VPTeamMemberList) {
          vptm.SelectedStaffName = OverallContactMap.get(vptm.SelectedStaffId);
        }
        if(VPTeamMemberList != null && VPTeamMemberList.size()>0 && selectItemSrNo >0 && VPTeamMemberList[selectItemSrNo-1].EngVPTeamMember.id != null) {
            //for (Opp_VP_TeamMember__c tm : [SELECT id from Opp_VP_TeamMember__c WHERE id =: VPTeamMemberList[selectItemSrNo-1].OppVPTeamMember.id])
                try{
                     Delete VPTeamMemberList[selectItemSrNo-1].EngVPTeamMember;                
                     NoOfAddedTeamMemberButNotSaved++;
                }catch (Exception ex){                           
                    // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error, System.Label.HL_VP_TeamMember_StopChange));
                   isErrorOnSave = true;
                   Apexpages.addMessages(ex);                    
                }        
        }
        if(VPTeamMemberList != null && VPTeamMemberList.size() >= selectItemSrNo && AddedTeamMemberIdSet != null && AddedTeamMemberIdSet.contains(VPTeamMemberList[selectItemSrNo-1].SelectedStaffId)){
            AddedTeamMemberIdSet.remove(VPTeamMemberList[selectItemSrNo-1].SelectedStaffId);            
            tempVPTMSet.remove(VPTeamMemberList[selectItemSrNo-1].SelectedStaffId);
        }
        if(selectItemSrNo != null && VPTeamMemberList != null && VPTeamMemberList.size() >= selectItemSrNo )
            VPTeamMemberList.remove(selectItemSrNo-1);          
        Integer i = 1;      
        for (VPTeamMember vptm : VPTeamMemberList) {
            if(vptm.EngVPTeamMember.Id == null || (vptm.EngVPTeamMember.Id != null && !vptm.EngVPTeamMember.Is_Team_Member_Deactivated_Once__c))
              IsSaveRequired = true;
            vptm.SrNo = i++;            
        }        
        return null;
    }
    
    public PageReference SaveTeamMembers() {        
        try{
            Map<Id, Eng_VP_TeamMember__c> MapofTM = new Map<Id, Eng_VP_TeamMember__c>([SELECT id, staff__c, staff__r.Name, Role__c, Start_Date__c, End_Date__c, Team_Member_Status__c, Engagement_VP_Position__c, Is_Team_Member_Deactivated_Once__c FROM Eng_VP_TeamMember__c  WHERE Engagement_VP_Position__c =: CurrentPositionId]);
        
            isErrorOnSave = false;
            List<Eng_VP_TeamMember__c> EngVPTeamMeberList = new List<Eng_VP_TeamMember__c>();
            Eng_VP_TeamMember__c tempItem; 
            Map<Id,Eng_VP_TeamMember__c> ExistingTMMap = new Map<Id,Eng_VP_TeamMember__c>();
            String memberStr2 = '';        
            Map<Id, sObject> mapIdExistingSet = new Map<Id,sObject>();
        
            for (VPTeamMember vptm : VPTeamMemberList) {
                tempItem = new Eng_VP_TeamMember__c();
                tempItem = vptm.EngVPTeamMember;
                tempItem.Staff__c = vptm.SelectedStaffId;
                if(OverallContactMap.containsKey(vptm.SelectedStaffId)) {
                    vptm.SelectedStaffName = OverallContactMap.get(vptm.SelectedStaffId);
                }
                else {
                    OverallContactMap.put(vptm.SelectedStaffId, vptm.SelectedStaffName);
                }
                tempItem.Name = vptm.SelectedStaffName;
                if(tempItem.Id != null)
                {
                    ExistingTMMap.put(tempItem.Id, tempItem);
                }
                else
                    EngVPTeamMeberList.add(tempItem);           
                
                if(vptm.EngVPTeamMember.Start_Date__c  != null && vptm.EngVPTeamMember.End_Date__c !=null && vptm.EngVPTeamMember.Start_Date__c > vptm.EngVPTeamMember.End_Date__c) {
                  memberStr2 += vptm.SelectedStaffName +', ';
                }
            }
            if(memberStr2.contains(',')) {
                memberStr2 = memberStr2.removeEnd(', ');
                memberStr2 +='.';
                DmlException e = new DmlException();
                e.setMessage('Start date can not be after end date, Please correct dates for: '+memberStr2);
                throw e;        
            }
            List<Eng_VP_TeamMember__c> VPTMstoUpdate = new List<Eng_VP_TeamMember__c>();
            for(Eng_VP_TeamMember__c EngVPTM : [Select id From Eng_VP_TeamMember__c WHERE ID IN : ExistingTMMap.KeySet()])
                if(ExistingTMMap.containsKey(EngVPTM.id))
                    VPTMstoUpdate.add(ExistingTMMap.get(EngVPTM.Id));
            if (VPTMstoUpdate.size() > 0) {
                update VPTMstoUpdate;            
            }
            if (EngVPTeamMeberList.size() > 0) {
                upsert EngVPTeamMeberList;            
            }
            isChangesSave = true;
            FetchLatestTeamMembers(); 
        }
        catch (Exception e) {
            isErrorOnSave = true;
            isChangesSave = false;
            Apexpages.addMessages(e);
        }    
        return null;
    }
    
    public class VPTeamMember{
        
        public Integer SrNo{get;set;}
        public Id SelectedStaffId{get;set;}
        public String SelectedStaffName{get;set{SelectedStaffName = value;}}
        public Eng_VP_TeamMember__c EngVPTeamMember{get;set;}
        
        public VPTeamMember(Integer SrNumber, Eng_VP_TeamMember__c EngVPTeamMemberParam )
        {
            EngVPTeamMember = new Eng_VP_TeamMember__c();
            if(EngVPTeamMemberParam != null) {
                EngVPTeamMember = EngVPTeamMemberParam;
                SelectedStaffId = EngVPTeamMemberParam.Staff__c;
                SelectedStaffName = EngVPTeamMemberParam.Staff__r.Name;             
            }
            SrNo = SrNumber;
        }            
    }
    
    // To verify if logged in user is member of Group Portfolio Staffing/Portfolio Supervisor. 
    private void VerifyAccessibilityToTM(){      
      
        UserCanCreateModifyTM = false;
        UserCanDeleteTM = false;      
     
        if(HL_OpportunityViewController.IsUserMemberofPVSuperVisorOrItschildGroup())
        {
            UserCanCreateModifyTM = true;
            UserCanDeleteTM = true;            
        }
        if(UserCanDeleteTM) {
            UserCanDeleteTM = false;
            for (permissionsetAssignment psa : [SELECT PermissionSet.Name 
                             FROM permissionsetAssignment 
                             WHERE AssigneeId =: UserInfo.getUserId() AND PermissionSet.Name =: Label.HL_PVPermissionSetName])
            {
                UserCanDeleteTM = true;                      
            }
        }
        if(!UserCanCreateModifyTM) {
            Engagement__c Eng = new Engagement__c (Id = EngagementId);
            UserCanCreateModifyTM = HL_EngagementViewController.VerifyUserforVP(Eng, 'EngagementPV');
        }
        
        for(User usr : [SELECT Id,Name, Profile.Name from User Where Id =: userinfo.getUserId()])       
        {          
            if(usr.Profile.Name == 'System Administrator')     
            {       
                UserCanCreateModifyTM = true;  
                UserCanDeleteTM = true;      
            }       
            if(usr.Profile.Name == 'System Administrator (Read Only)')     
            {       
                UserCanCreateModifyTM = false;  
                UserCanDeleteTM = false;      
            } 
        }          
    }
    public PageReference Backtovp() {
        string url = '/' + EngVpId ;                        
        PageReference result = new PageReference(url);
        result.setRedirect(true);
        return result;
        
    }
    public PageReference VoidPosition() {
        string url = '/apex/HL_EngValuationPeriodPositionCancel?id=' + CurrentPositionId;                
        if (!String.isBlank(HL_PageUtility.GetParameter('retURL')))
           url = url + '&retURL=' + HL_PageUtility.GetParameter('retURL');
        PageReference result = new PageReference(url);
        result.setRedirect(true);
        return result;        
    }
    
    public PageReference DeleteRecord() { 
        Try{            
            if(EngVPPosition!= null)
                delete EngVPPosition;
            
        }catch(Exception e)
        {
            ApexPages.addmessages(e);
            return null;
        }
        return Backtovp();
    }     
    public Double offset{get{
        TimeZone tz = UserInfo.getTimeZone();
        //Milliseconds to Day
        return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
    }} 
    public PageReference saveRc()
    {
        try{        
            
            if((EngVPPosition.Name == null || EngVPPosition.Name == '') && tempName != '' && tempName != null)
                EngVPPosition.Name = tempName;
            update EngVPPosition;
        }catch(Exception e )
        {
            ApexPages.addMessages(e);
            return null;
        }        
        return new pageReference('/'+EngVPPosition.Id);
     }
     
    public void DummyReset(){         
        if(fieldAPI == 'Name'){
            EngVPPosition.Name = OriginalPosition.Name;
            tempName = OriginalPosition.Name;            
        }
        else if(fieldAPI == 'Asset_Classes__c')
            EngVPPosition.Asset_Classes__c = OriginalPosition.Asset_Classes__c ;
        else if(fieldAPI == 'Notes__c')
            EngVPPosition.Notes__c = OriginalPosition.Notes__c;
        else if(fieldAPI == 'Report_Fee__c')
            EngVPPosition.Report_Fee__c = OriginalPosition.Report_Fee__c;        
        else if(fieldAPI == 'Status__c')
            EngVPPosition.Status__c = OriginalPosition.Status__c;
        else if(fieldAPI == 'Company__c')
            EngVPPosition.Company__c = OriginalPosition.Company__c;         
    } 
}