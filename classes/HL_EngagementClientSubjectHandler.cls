public class HL_EngagementClientSubjectHandler {
    private boolean isExecuting = false;
    private integer batchSize = 0;
    public boolean IsTriggerContext{get{ return isExecuting;}}
    public static Boolean isAfterInsertFlag = false;
    public static Boolean isAfterUpdateFlag = false;
    public static Boolean isAfterDeleteFlag = false;
    
    public HL_EngagementClientSubjectHandler(boolean isExecuting, integer size){
        isExecuting = isExecuting;
        batchSize = size;
    }
    
    public void OnAfterInsert(List<Engagement_Client_Subject__c> ecsList){
        if(!isAfterInsertFlag){
            isAfterInsertFlag = true;
            UpdatePublicPrivate(ecsList);
        }
    }
    
    public void OnAfterUpdate(List<Engagement_Client_Subject__c> ecsList){
        if(!isAfterUpdateFlag){
            isAfterUpdateFlag = true;
            UpdatePublicPrivate(ecsList);
        }
    }
    
    public void OnAfterDelete(List<Engagement_Client_Subject__c> ecsList){
         if(!isAfterDeleteFlag){
            isAfterDeleteFlag = true;
            UpdatePublicPrivate(ecsList);
         }
    }
    
    private void UpdatePublicPrivate(List<Engagement_Client_Subject__c> ecsList){
        //List of Engagements to Update
        List<Engagement__c> engsToUpdate = new List<Engagement__c>();
        //Set of Eng IDs
        Set<Id> engs = new Set<Id>();
        //Get a Map of Engs with Client/Subject ownership
        Map<Id,Set<String>> engsToOwnership = new Map<Id,Set<String>>();
        Set<String> currentVals;
        for(Engagement_Client_Subject__c ecs : ecsList)
            engs.Add(ecs.Engagement__c);
        if(engs.size() > 0){
            //Get all related Client/Subjects
            for(Engagement_Client_Subject__c cs : [SELECT Engagement__c, Client_Subject__r.Ownership FROM Engagement_Client_Subject__c WHERE Engagement__c IN:engs]){ 
                if(!engsToOwnership.containsKey(cs.Engagement__c))
                    engsToOwnership.put(cs.Engagement__c, new Set<String> {cs.Client_Subject__r.Ownership});
                else{
                    currentVals = engsToOwnership.get(cs.Engagement__c);
                    currentVals.add(cs.Client_Subject__r.Ownership);
                    engsToOwnership.put(cs.Engagement__c, currentVals);
                } 
            }

            //Update the Engagements
            for(Id engId : engsToOwnership.keySet()){
                currentVals = engsToOwnership.get(engId);
                engsToUpdate.add(new Engagement__c(
                                                   Id = engId, 
                                                   Public_Or_Private__c = (currentVals.contains('Public Debt') || currentVals.contains('Public Equity') ? 'Public' : 'Private')
                                                  )
                                ); 
            }

            if(engsToUpdate.size() > 0)
                update engsToUpdate;
       }
    }
    public static void stopDeletion(List<Engagement_Client_Subject__c> deletingRecordList) {
         for(Engagement_Client_Subject__c  engObj : deletingRecordList){
          if(engObj.Primary__c ){             
             engObj.addError('Primary Clients and Subjects cannot be deleted');
            }
        }
    }
       
    public static void stopDuplicate(List < Engagement_Client_Subject__c > InsertingACSRecords) {
        set < ID > RelatedEngagementIdSet = new set < ID > ();
        set < ID > RelatedCompantyIdSet = new set < ID > ();
        for (Engagement_Client_Subject__c engObj1: InsertingACSRecords) {
            RelatedEngagementIdSet.add(engObj1.Engagement__c);
            RelatedCompantyIdSet.add(engObj1.Client_Subject__c);
        }
        map < id, Account > RelatedAccountMap = new map < Id, Account > ([SELECT id, Name FROM Account WHERE id IN : RelatedCompantyIdSet]);
        map < id, set < string >> EngagementWithRelatedExistingACSMap = new map < id, set < string >> ();

        Set < String > CombinationSet;
        for (Engagement__c eng : [SELECT ID, (SELECT id, Type__c, Client_Subject__c, Engagement__c, Role__c, Other_Related_Object_Id__c FROM Engagement_Clients_Subjects__r) FROM Engagement__c WHERE ID IN : RelatedEngagementIdSet]) {
            CombinationSet = new Set < String > ();
            for (Engagement_Client_Subject__c acs : eng.Engagement_Clients_Subjects__r) {
                string role = '';
                string relatedObject = '';
                if (acs.Role__c != null) {
                    role = acs.Role__c ;
                }
                
                if (acs.Other_Related_Object_Id__c != null) {
                    relatedObject =  String.valueOf(acs.Other_Related_Object_Id__c);
                }
                
                CombinationSet.add(acs.type__c + '_' + acs.Client_Subject__c + '_' + role + '_' + relatedObject);
            }
            EngagementWithRelatedExistingACSMap.put(eng.Id, CombinationSet);
        }

        for (Engagement_Client_Subject__c ACSRecord : InsertingACSRecords) {       
            string role = '';
            string relatedObject = '';
            if (ACSRecord.Role__c != null) {
                role = ACSRecord.Role__c ;
            }
            
            if (ACSRecord.Other_Related_Object_Id__c != null) {
                relatedObject =  String.valueOf(ACSRecord.Other_Related_Object_Id__c);
            }
            If(EngagementWithRelatedExistingACSMap.containsKey(ACSRecord.Engagement__c) && EngagementWithRelatedExistingACSMap.get(ACSRecord.Engagement__c) != null 
               && EngagementWithRelatedExistingACSMap.get(ACSRecord.Engagement__c).contains(ACSRecord.type__c + '_' + ACSRecord.Client_Subject__c + '_' + role + '_' + relatedObject) 
               && RelatedAccountMap.containsKey(ACSRecord.Client_Subject__c)) {
                ACSRecord.addError('Company Name : ' + '\'' +RelatedAccountMap.get(ACSRecord.Client_Subject__c).Name + '\'' + ' already exists as an Additional Client/Subject');
            }
        }
    }    
    
    public static void validateDuplicateRecord(List<Engagement_Client_Subject__c> EnggClientNewList, Map <Id,Engagement_Client_Subject__c> EnggClientOldMap) {
        Map<String, Engagement_Client_Subject__c> EnggClientUniqueMap = new Map<String, Engagement_Client_Subject__c>();
        set<Id> EngagementIdSet = new Set<Id>();
        set<Engagement_Client_Subject__c> BulkyLoaderSet = new Set<Engagement_Client_Subject__c>();
        for (Engagement_Client_Subject__c EnggClient : EnggClientNewList){             
            //Check for duplicate Primary Address in same transaction
            if (((EnggClient.Client_Subject__c+''+EnggClient.Engagement__c+''+EnggClient.Type__c) !=null) && 
                ((EnggClient.Client_Subject__c+''+EnggClient.Engagement__c+''+EnggClient.Type__c) != 
                    (EnggClientOldMap.get(EnggClient.Id).Client_Subject__c+''+EnggClientOldMap.get(EnggClient.Id).Engagement__c+''+EnggClientOldMap.get(EnggClient.Id).Type__c)))
            {
                string role = '';
                string relatedObject = '';
                if (EnggClient.Role__c != null) {
                    role = EnggClient.Role__c ;
                }
                if (EnggClient.Other_Related_Object_Id__c != null) {
                    relatedObject =  String.valueOf(EnggClient.Other_Related_Object_Id__c);
                }
                BulkyLoaderSet.add(EnggClient);
                // Check for existing Primary Address       
                if (EnggClientUniqueMap.containsKey(EnggClient.Client_Subject__c+''+EnggClient.Engagement__c+''+EnggClient.Type__c+''+role+''+relatedObject)){
                    EnggClient.addError('Company Name : ' + '\'' + EnggClient.Client_Subject__r.Name+'\''+ 'with this Type already exists');
                }else{
                    EnggClientUniqueMap.put((EnggClient.Client_Subject__c+''+EnggClient.Engagement__c+''+EnggClient.Type__c+''+role+''+relatedObject), EnggClient);
                    EngagementIdSet.add(EnggClient.Engagement__c);
                }
            }            
        }
        
        for (Engagement_Client_Subject__c enggclientRecord  : [SELECT Client_Subject__c, Engagement__c,Type__c,Client_Subject__r.Name, Role__c, Other_Related_Object_Id__c
                                                               FROM Engagement_Client_Subject__c
                                                               WHERE Engagement__c IN :EngagementIdSet AND ID NOT IN : BulkyLoaderSet limit : Limits.getLimitQueryRows()]){    
            string role = '';
            string relatedObject = '';
            if (enggclientRecord.Role__c != null) {
                role = enggclientRecord.Role__c ;
            }
            if (enggclientRecord.Other_Related_Object_Id__c != null) {
                relatedObject =  String.valueOf(enggclientRecord.Other_Related_Object_Id__c);
            }
            if(EnggClientUniqueMap.containsKey(enggclientRecord.Client_Subject__c+''+enggclientRecord.Engagement__c+''+enggclientRecord.Type__c+''+role+''+relatedObject)) {
                Engagement_Client_Subject__c duplicateRecord = EnggClientUniqueMap.get(enggclientRecord.Client_Subject__c+''+enggclientRecord.Engagement__c+''+enggclientRecord.Type__c+''+role);
                duplicateRecord.addError('Company Name : ' + '\''+enggclientRecord.Client_Subject__r.Name+'\''+ 'with this Type already exists');
            }
        }
    }
}