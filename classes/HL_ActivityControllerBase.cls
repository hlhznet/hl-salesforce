public virtual class HL_ActivityControllerBase {
    public Contact UserContactRecord {get{
       if(userContactRecord == null)
           userContactRecord = HL_Contact.GetByUserId();
        
       return userContactRecord;   
    } set;}
    
    public Boolean HasInternalSupervisorAccess
    {
        get{
            if (hasInternalSupervisorAccess == null) 
                hasInternalSupervisorAccess = HL_SecurityUtility.IsSysAdmin() || HL_Group.IsInGroup('Activity_Internal_Supervisor_Access');
            
          return HasInternalSupervisorAccess;
        }
        set;
    }
    
    public Boolean HasInternalAccess 
    {
        get
        {
            if (hasInternalAccess == null) 
                hasInternalAccess = HL_SecurityUtility.IsSysAdmin() || HL_Group.IsInGroup('Activity_Internal_Supervisor_Access') || HL_Group.IsInGroup('Activity_Internal_Access');
            
          return hasInternalAccess;
        } 
        set;
    }
    
    public Boolean hasMentorActivityAccess
    {
        get
        {
            if (hasMentorActivityAccess == null) 
                hasMentorActivityAccess = HL_SecurityUtility.IsSysAdmin() || HL_Group.IsInGroup('Mentor_Activity_Access');
          return hasMentorActivityAccess;
        } 
        set;
    }    
     
    public Id EntityId 
    {
        get
        {
            if (String.isBlank(entityId))
                entityId = userInfo.getUserId();
            return entityId;
        } 
        set;
    }
    
    public String EntityType 
    {
        get
        {
            return !String.isBlank(EntityId) ? HL_Utility.GetObjectTypeById(EntityId) : '';
        }
    }
    
    public String RetEntity 
    {
        get
        {
            return EntityType == 'User' ? '/' : EntityId;
        }
    }
    
    public Set<Id> Supervisors 
    {
        get
        {
            if (supervisors == null) {
                supervisors = new Set<Id>();
                //Get the Supervisors to provide appropriate view/edit rights
                for (Delegate_Public_Group__c dpg : HL_DelegatePublicGroup.GetSupervisors())
                    supervisors.add(dpg.Banker__c);
            }
            return supervisors;
        } 
        set;
    }
    
    //For Companies(Accounts) We Want to use what_id, for Contacts use who_id
    public PageReference NewActivity() {
        PageReference result = new PageReference('/apex/HL_ActivityEvent?' + (HL_Utility.GetObjectTypeById(EntityId) == 'Account' ? 'what_id=' : 'who_id=') + EntityId + '&retURL=' + EntityId);
        return result;
    }
        
    public PageReference EditRecord() {
        string url = '/apex/HL_ActivityEvent?id=' + EntityId;                
        if (!String.isBlank(HL_PageUtility.GetParameter('retURL')))
           url = url + '&retURL=' + HL_PageUtility.GetParameter('retURL');
        PageReference result = new PageReference(url);
        result.setRedirect(true);
        return result;
        
    }
    
    public HL_Activity CreateActivityFromEvent(Id contactId, Id userId, Event e, Map<Id, Contact> primaryAttendeeMap, Set<Id> attendeeContactIdSet,  Set<Id> menteeContactIdSet) {
        HL_Activity a = new HL_Activity();
        Contact primaryAttendee = primaryAttendeeMap.Get(e.Primary_Attendee_Id__c);
        Id internalRecordTypeAccess;
        if(attendeeContactIdSet == null)
            attendeeContactIdSet = new Set<Id>();
        if(HasInternalSupervisorAccess){
            Schema.RecordTypeInfo internalRecordTypeInfo = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Internal ' + UserContactRecord.Line_of_Business__c);
            if(internalRecordTypeInfo != null)
              internalRecordTypeAccess = internalRecordTypeInfo.getRecordTypeId();
        }  
        a.Activity = e;
        a.ModifyAccess = contactId == e.Primary_Attendee_Id__c || 
                   userId == e.CreatedById ||
                   Supervisors.contains(e.CreatedById) ||
                    (!e.Private__c && e.Type <> 'Internal' && attendeeContactIdSet.contains(contactId)) ||
                  (primaryAttendee != null && this.Supervisors.contains(primaryAttendee.User__c)) || 
                  (internalRecordTypeAccess != null && e.RecordTypeId == internalRecordTypeAccess);
        a.ViewAccess = a.ModifyAccess || attendeeContactIdSet.contains(contactId);        
        a.PrimaryAttendee = e.Primary_Attendee__c == null ? e.CreatedBy.Name : e.Primary_Attendee__c;
        a.PrimaryContactId = e.Primary_External_Contact_Id__c;
        a.PrimaryContact = e.Primary_External_Contact__c;
        a.LinkId = e.ParentId__c == null ? e.Id : e.ParentId__c;
        if(e.Type == 'Internal Mentor Meeting') {
            a.ModifyAccess = (attendeeContactIdSet.contains(contactId) && HL_Group.IsInGroup('Mentor_Activity_Access'));
            a.ViewAccess = a.ModifyAccess ||  HL_Group.IsInGroup('Mentor_Activity_Access') || (menteeContactIdSet != null && menteeContactIdSet.contains(contactId));  
        }           
        return a;
    }
    
    private Boolean HasLOBSupervisor(string lob) {
        Boolean result = false;
        List<User> supervisorList = new List<User>();
        
        for (Id id : this.Supervisors)
            supervisorList.add(new User(Id = id));
        
        List<Contact> supervisorContactList = HL_Contact.GetListByUsers(supervisorList);
        
        for (Contact s : supervisorContactList) {
            if (s.Line_of_Business__c == lob) {
                result = true;
                break;
            }
        }
        
        return result;
    }
}