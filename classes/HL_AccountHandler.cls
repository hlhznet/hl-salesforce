public with sharing class HL_AccountHandler  {
    private boolean IsExecuting = false;
    private integer BatchSize = 0;
    private static Map<Id, List<Account>> parentIdToAccountListMap;
    private static Map<Id, Set<Id>> parentIdToChildSetMap;

    public HL_AccountHandler(boolean executing, integer size) {
        IsExecuting = executing;
        BatchSize = size;
    }
    public void OnBeforeUpdate(List<Account> newAccountList , List<Account> oldAccountList,  Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap) { 
    // To populate clean status based on check box field value by Data hygiene user
    populateCleanStatus(newAccountList, newAccountMap, oldAccountMap); 
    } 
    
    public void OnBeforeInsert(List<Account> accountList) {
        Set<Id> parentIdSet = new Set<Id>();
        Map<Id, Id> accountIdToCapIQIdMatchMap = new Map<Id, Id>();

        for (Account a : accountList) {
            if (!String.isBlank(a.ParentId)){
                parentIdSet.add(a.ParentId);

                if(!String.isBlank(a.CapIQ_Company__c)){
                    accountIdToCapIQIdMatchMap.put(a.Id, a.CapIQ_Company__c);
                    accountIdToCapIQIdMatchMap.put(a.ParentId, a.CapIQ_Company__c);
                }
            }
        }

        if(parentIdSet.size() > 0)
            UpdateParentFieldOnChildBeforeInsert(accountList, parentIdSet);

        if(accountIdToCapIQIdMatchMap.size() > 0)
            UpdateRelatedCompanyCapIQIDs(null, accountIdToCapIQIdMatchMap);

        IsEUForCF(accountList);
    }

    private void UpdateParentFieldOnChildBeforeInsert(List<Account> accountList, Set<Id> parentIdSet) {
        //Map used to store the filtered Account
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Ultimate_Parent__c FROM Account WHERE Id IN:parentIdSet]);

        for (Account a : accountList) {
            if (!String.isBlank(a.ParentId) && accountMap.containsKey(a.ParentId)) {
                //If Ultimate Parent Id is null then update the Child with its Parent Account Id as Ultimate Parent.
                a.Ultimate_Parent__c = (!String.isBlank(accountMap.get(a.ParentId).Ultimate_Parent__c))
                    ? accountMap.get(a.ParentId).Ultimate_Parent__c
                    : a.ParentId;
            }
        }
    }

    private void UpdateRelatedCompanyCapIQIDs(Set<Id> accountIdSet, Map<Id, Id> accountIdToCapIQIdMatchMap){
        List<Account> accountUpdateList = new List<Account>();
        //Map to store the parent and child ids to the capiq id
        List<Account> capIQAccountList = new List<Account>();

        //Get all accounts part of the CapIQ Updates/Assignment
        for(Account a : [SELECT ParentId, CapIQ_Company__c
                         FROM Account
                         WHERE CapIQ_Company__c IN: accountIdToCapIQIdMatchMap.values()]){
                             accountIdToCapIQIdMatchMap.put(a.Id, a.CapIQ_Company__c);
                             if(!String.isBlank(a.ParentId))
                                 accountIdToCapIQIdMatchMap.put(a.ParentId, a.CapIQ_Company__c);
                             capIQAccountList.add(a);
                         }

        if(accountIdSet == null)
            accountIdSet = new Set<Id>();

        //Get all of the related accounts without a CapIQ Company Assignment
        for(Account a : [SELECT ParentId, CapIQ_Company__c
                         FROM Account
                         WHERE (Id IN: accountIdToCAPIQIdMatchMap.keySet() OR ParentId IN: accountIdToCAPIQIdMatchMap.keySet()) AND CapIQ_Company__c = null AND Id NOT IN: accountIdSet]){
                             //Verify we are not updating one of the accounts already in the trigger context
                             Id capIQId = accountIdToCapIQIdMatchMap.get(a.Id);

                             if(String.isBlank(capIQId) && !String.isBlank(a.ParentId))
                                 capIQId = accountIdToCapIQIdMatchMap.get(a.ParentId);

                             if(!String.isBlank(capIQId)){
                                 a.CapIQ_Company__c = capIQId;
                                 accountUpdateList.add(a);
                             }
                         }

        if(accountUpdateList.size() > 0)
            update accountUpdateList;
    }

    //Update ultimate parent field on child records.
    public void OnAfterUpdate(Map<Id, Account> oldAccountMap, Map<Id, Account> newAccountMap) {
        this.UpdateParentFieldOnChildAfterUpdate(oldAccountMap, newAccountMap);

        Map<Id, Id> accountIdToCapIQIdMatchMap = new Map<Id, Id>();
        //Check if the CapIQ Company Link has been Updated
        for(Account a : newAccountMap.values()){
            if(!String.isBlank(a.CapIQ_Company__c) && oldAccountMap.get(a.Id).CapIQ_Company__c != newAccountMap.get(a.Id).CapIQ_Company__c){
                accountIdToCapIQIdMatchMap.put(a.Id, a.CapIQ_Company__c);
                if(!String.isBlank(a.ParentId))
                    accountIdToCapIQIdMatchMap.put(a.ParentId, a.CapIQ_Company__c);
            }
        }

        if(accountIdToCapIQIdMatchMap.size() > 0)
            UpdateRelatedCompanyCapIQIDs(newAccountMap.keySet(), accountIdToCapIQIdMatchMap);
    }

    private void UpdateParentFieldOnChildAfterUpdate(Map<Id, Account> oldAccountMap, Map<Id, Account> newAccountMap) {
        Set<Id> parentIdSet = new Set<Id>();
        Set<Id> ultimateParentIdSet = new Set<Id>();

        parentIdToAccountListMap = new Map<Id, List<Account>>();
        Map<Id, Account> accountMap = new Map<Id, Account>();

        //getting the utimate id to get all the related child records.
        for (Account a : newAccountMap.values()) {
            if (a.ParentId != oldAccountMap.get(a.Id).ParentId) {
                if (a.ParentId == null)
                    parentIdSet.add(a.Id);
                else if (a.ParentId != null && !newAccountMap.containsKey(a.ParentId))
                    parentIdSet.add(a.ParentId);

                if (!String.isBlank(oldAccountMap.get(a.Id).Ultimate_Parent__c))
                    ultimateParentIdSet.add(oldAccountMap.get(a.Id).Ultimate_Parent__c);
                else if (oldAccountMap.get(a.Id).ParentId == null)
                    ultimateParentIdSet.add(a.Id);

                if (!String.isBlank(a.ParentId) && parentIdToAccountListMap.containsKey(a.ParentId))
                    parentIdToAccountListMap.get(a.ParentId).add(a);
                else if (!String.isBlank(a.ParentId))
                    parentIdToAccountListMap.put(a.ParentId, new List<Account> {a});

                accountMap.put(a.Id, a);
            }
        }

        if (!accountMap.isEmpty())
            updateUltimateParent(accountMap, parentIdSet, ultimateParentIdSet);
    }

    //Update the Ultimate Parent Field on Child Records
    public void OnBeforeDelete(Map<Id, Account> oldAccountMap) {
        this.UpdateParentFieldOnChildBeforeDelete(oldAccountMap);
    }

    private void UpdateParentFieldOnChildBeforeDelete(Map<Id, Account> oldAccountMap) {
        parentIdToAccountListMap = new Map<Id, List<Account>>();
        parentIdToChildSetMap = new Map<Id, Set<Id>>();
        Set<Id> ultimateParentIdSet = new Set<Id>();

        //getting the utimate id to get all the related child records.
        for (Account a : oldAccountMap.values()) {
            //checks to set ultimate Id if not null else assigns the Parent Id as the Ultimate Id
            if (!String.isBlank(a.Ultimate_Parent__c))
                ultimateParentIdSet.add(oldAccountMap.get(a.Id).Ultimate_Parent__c);
            else
                ultimateParentIdSet.add(a.Id);
        }

        for (Account a : [SELECT
                          ParentId,
                          Ultimate_Parent__c
                          FROM Account
                          WHERE ParentId IN: oldAccountMap.keySet()
                          AND Id NOT IN: oldAccountMap.keySet()]) {
                              if (parentIdToChildSetMap.containsKey(a.ParentId))
                                  parentIdToChildSetMap.get(a.ParentId).add(a.Id);
                              else
                                  parentIdToChildSetMap.put(a.ParentId, new Set<Id> {a.Id});
                          }

        for (Account a : [SELECT
                          Ultimate_Parent__c,
                          ParentId
                          FROM Account
                          WHERE Ultimate_Parent__c IN: ultimateParentIdSet
                          AND Id NOT IN: oldAccountMap.keySet()]) {
                              if (parentIdToAccountListMap.containsKey(a.ParentId))
                                  parentIdToAccountListMap.get(a.ParentId).add(a);
                              else
                                  parentIdToAccountListMap.put(a.ParentId, new List<Account> {a});
                          }
    }

    //Update the Ultimate Parent Field on Child Records
    public void OnAfterDelete(Map<Id, Account> oldAccountMap) {
        this.UpdateParentFieldOnChildAfterDelete(oldAccountMap);
        this.HandleMergeScenarios(oldAccountMap);
    }

    private void UpdateParentFieldOnChildAfterDelete(Map<Id, Account> oldAccountMap) {
        List<Account> accountUpdateList = new List<Account>();

        for (Account a : oldAccountMap.values()) {
            if (parentIdToChildSetMap.containsKey(a.Id)) {
                for (Id childId : parentIdToChildSetMap.get(a.Id)) {
                    accountUpdateList.add(new Account(Id = childId, Ultimate_Parent__c = null));
                    if (parentIdToAccountListMap.containsKey(childId)) {
                        accountUpdateList.addAll(updateChildRecordUltimateParent(parentIdToAccountListMap.get(childId), oldAccountMap.keySet(), childId));
                    }
                }
            }
        }

        //Update the Account and its child record with the Ultimate Parent Id
        if (!accountUpdateList.isEmpty())
            update accountUpdateList;
    }

    //Update the Ultimate Parent Field on Child Records
    private void UpdateUltimateParent(Map<Id, Account> accountMap, Set<Id> parentIdSet, Set<Id> ultimateParentIdSet) {
        List<Account> topParentAccountList = [SELECT Id, Ultimate_Parent__c, ParentId FROM Account WHERE Id IN: parentIdSet ];
        List<Account> accountUpdateList = new List<Account>();

        for (Account a : [SELECT
                          Ultimate_Parent__c,
                          ParentId
                          FROM Account
                          WHERE Ultimate_Parent__c IN: ultimateParentIdSet
                          AND Id NOT IN: accountMap.keySet()]) {
                              accountMap.put(a.Id, a);
                              if (a.ParentId != null) {
                                  if (parentIdToAccountListMap.containsKey(a.ParentId))
                                      parentIdToAccountListMap.get(a.ParentId).add(a);
                                  else
                                      parentIdToAccountListMap.put(a.ParentId, new List<Account> {a});
                              }
                          }
        //Iterate on updated Account records and calls the method to get the child record to update with Ultimate Parent and Parent Id.
        for (Account a : topParentAccountList) {
            if (a.ParentId != null && parentIdToAccountListMap.containsKey(a.Id)) {
                accountUpdateList.addAll(updateChildRecordUltimateParent(parentIdToAccountListMap.get(a.Id),
                                                                         parentIdSet,
                                                                         a.Ultimate_Parent__c));
            }
            else if (a.ParentId == null && parentIdToAccountListMap.containsKey(a.Id)) {
                accountUpdateList.add(new Account(Id = a.Id, Ultimate_Parent__c = null));
                accountUpdateList.addAll(updateChildRecordUltimateParent(parentIdToAccountListMap.get(a.Id),
                                                                         parentIdSet,
                                                                         a.Id));
            }

            else if (a.ParentId == null) {
                accountUpdateList.add(new Account(Id = a.Id, Ultimate_Parent__c = null));
            }
        }
        //Update the Account and its child record with the Ultimate Parent Id and its Parent Id.
        if (!accountUpdateList.isEmpty())
            update accountUpdateList;
    }

    //Update ultimate parent field on child records
    private List<Account> UpdateChildRecordUltimateParent(List<Account> accountList, Set<Id> setParentChangeId, Id ultimateId) {
        List<Account> accountUpdateList = new List<Account>();//List to return child Account records to update.
        //getting the utimate id to get all the related child records.
        for (Account a : accountList) {
            //check if Account Record contains any child records
            if (!setParentChangeId.contains(a.Id)) {
                if (!String.isBlank(a.ParentId) && parentIdToAccountListMap.containsKey(a.Id)) {
                    accountUpdateList.add(new Account(Id = a.Id, Ultimate_Parent__c = ultimateId));
                    //calls the method itself in recursion till it gets all its child
                    accountUpdateList.addAll(updateChildRecordUltimateParent(parentIdToAccountListMap.get(a.Id), setParentChangeId, ultimateId));
                }
                else if (!String.isBlank(a.ParentId)) {
                    accountUpdateList.add(new Account(Id = a.Id, Ultimate_Parent__c = ultimateId));
                }
            }
        }
        return accountUpdateList;//return list of child Accounts of particular parents to update its Ultimate Parent and its Parent
    }

    private void HandleMergeScenarios(Map<Id, Account> oldAccountMap){
        MergedData mergedData = this.GetMergedData(oldAccountMap);
        this.UpdateEventRecords(mergedData);
    }

    private MergedData GetMergedData(Map<Id, Account> oldAccountMap) {
        MergedData mergedData = new MergedData();

        for (Account a : oldAccountMap.values()) {
            if (!String.isBlank(a.MasterRecordId)) {
                if (mergedData.MasterIdToMergedIdsMap.get(a.MasterRecordId) == null)
                    mergedData.MasterIdToMergedIdsMap.put(a.MasterRecordId, new Set<Id> {a.Id});
                else {
                    Set<Id> mergedIdSet = mergedData.MasterIdToMergedIdsMap.get(a.MasterRecordId);
                    mergedIdSet.add(a.Id);
                    mergedData.MasterIdToMergedIdsMap.put(a.MasterRecordId, mergedIdSet);
                }

                mergedData.MergedAccountMap.put(a.Id, a);
            }
        }

        //Create a Map of Merged Account Id to New Account, The New Account Must be Queried for as we need other Account Fields from the Record
        for (Account a : [SELECT Name FROM Account WHERE Id IN: mergedData.MasterIdToMergedIdsMap.keySet()]) {
            Set<Id> mergedIdSet = mergedData.MasterIdToMergedIdsMap.get(a.Id);
            for (Id mergedId : mergedIdSet)
                mergedData.MergedIdToNewAccountMap.put(mergedId, a);
        }

        return mergedData;
    }

    //Handle Merge of Contacts to Update the Event Records
    private List<Event> UpdateEventRecords(MergedData mergedData) {
        List<Event> eventUpdateList = new List<Event>();

        if (mergedData.MergedAccountMap.size() > 0) {
            for (Event e : [SELECT Primary_External_Contact_Company_Id__c
                            FROM Event
                            WHERE Primary_External_Contact_Company_Id__c IN: mergedData.MergedAccountMap.keySet()]) {
                                Account masterAccount = mergedData.MergedIdToNewAccountMap.get(e.Primary_External_Contact_Company_Id__c);

                                if (masterAccount != null) {
                                    e.Primary_External_Contact_Company_Id__c = masterAccount.Id;
                                    eventUpdateList.add(e);
                                }

                            }
        }

        if (eventUpdateList.size() > 0)
            update eventUpdateList;

        return eventUpdateList;
    }

    private class MergedData {
        public Map<Id, Account> MergedAccountMap {get; set;}
        public Map<Id, Account> MergedIdToNewAccountMap {get; set;}
        public Map<Id, Set<Id>> MasterIdToMergedIdsMap {get; set;}

        public MergedData() {
            this.MergedAccountMap = new Map<Id, Account>();
            this.MergedIdToNewAccountMap = new Map<Id, Account>();
            this.MasterIdToMergedIdsMap = new Map<Id, Set<Id>>();
        }
    }

    //The Set below is a predetermined list that CF has given to classify an Account as EU based on BillingCountry
    Set<String> EUCountries = new Set<String>{'armenia',
                                                'egypt',
                                                'iran',
                                                'iraq',
                                                'israel',
                                                'jordan',
                                                'kuwait',
                                                'lebanon',
                                                'oman',
                                                'palestine',
                                                'qatar',
                                                'saudi arabia',
                                                'syria',
                                                'turkey',
                                                'united arab emirates',
                                                'yemen',
                                                'bahrain',
                                                'algeria',
                                                'angola',
                                                'benin',
                                                'botswana',
                                                'burkina faso',
                                                'burundi',
                                                'cameroon',
                                                'cape verde',
                                                'central african republic',
                                                'chad',
                                                'comoros',
                                                'democratic republic of the congo',
                                                'djibouti',
                                                'egypt',
                                                'equatorial guinea',
                                                'eritrea',
                                                'ethiopia',
                                                'gabon',
                                                'gambia',
                                                'ghana',
                                                'guinea',
                                                'guinea-bissau',
                                                'ivory coast',
                                                'kenya',
                                                'lesotho',
                                                'liberia',
                                                'libya',
                                                'madagascar',
                                                'malawi',
                                                'mali',
                                                'mauritania',
                                                'mauritius',
                                                'mayotte',
                                                'morocco',
                                                'mozambique',
                                                'namibia',
                                                'niger',
                                                'nigeria',
                                                'republic of the congo',
                                                'rwanda',
                                                'réunion',
                                                'saint helena, ascension and tristan da cunha',
                                                'senegal',
                                                'seychelles',
                                                'sierra leone',
                                                'somalia',
                                                'south africa',
                                                'south sudan',
                                                'sudan',
                                                'swaziland',
                                                'são tomé and príncipe',
                                                'tanzania',
                                                'togo',
                                                'tunisia',
                                                'uganda',
                                                'western sahara',
                                                'zambia',
                                                'zimbabwe',
                                                'deutschland',
                                                'isle of man',
                                                'russia',
                                                'albania',
                                                'andorra',
                                                'armenia',
                                                'austria',
                                                'azerbaijan',
                                                'belarus',
                                                'belgium',
                                                'bosnia and herzegovina',
                                                'bulgaria',
                                                'croatia',
                                                'cyprus',
                                                'czech republic',
                                                'denmark',
                                                'estonia',
                                                'finland',
                                                'france',
                                                'georgia',
                                                'germany',
                                                'greece',
                                                'hungary',
                                                'iceland',
                                                'ireland',
                                                'italy',
                                                'latvia',
                                                'liechtenstein',
                                                'lithuania',
                                                'luxembourg',
                                                'macedonia',
                                                'malta',
                                                'moldova',
                                                'monaco',
                                                'montenegro',
                                                'netherlands',
                                                'norway',
                                                'poland',
                                                'portugal',
                                                'romania',
                                                'san marino',
                                                'serbia',
                                                'slovakia',
                                                'slovenia',
                                                'spain',
                                                'sweden',
                                                'switzerland',
                                                'ukraine',
                                                'united kingdom',
                                                'vatican city'};

    //This method updates a checkbox on the Account to reflect CF's EU classification
    public void IsEUForCF(List<Account> accList){
        List<Account> accsToUpdate = new List<Account>();
        for(Account a : accList){
            String country = a.BillingCountry;
            if(!String.isBlank(country) && EUCountries.contains(country.toLowerCase())){
                    a.Is_Client_EU_CF__c = TRUE;
            }
            else
                    a.Is_Client_EU_CF__c = FALSE;
                accsToUpdate.add(a);
        }

    }
  
    private void populateCleanStatus(List<Account> newAccountList, Map<Id, Account> newAccountMap, Map<Id, Account> oldAccountMap){
    for(Account acc: newaccountList)
        {
         if(acc.Clean_Job_By_Pass__c)
         {
             acc.CleanStatus = 'Skipped';
         }         
         else if(oldAccountMap.get(acc.id).Clean_Job_By_Pass__c && !newAccountMap.get(acc.id).Clean_Job_By_Pass__c){
             // 'Pending' is API vlaue for clean status 'Not Compared'
             acc.CleanStatus = 'Pending';
         }                  
        }
    } 
}