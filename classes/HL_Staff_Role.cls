public with sharing class HL_Staff_Role {
    public enum RoleType {Opportunity, Engagement}
    public enum RoleLOB {CF, FAS, FR, SC}
    //Returns the possible roles by Opportunity/Engagement and Related LOB
    //Include FS__c for Business Rule Checks
    public static List<Staff_Role__c> GetRoles(RoleType rt, RoleLOB lob){
        Boolean opportunity = rt == RoleType.Opportunity;
        Boolean engagement = rt == RoleType.Engagement;
        Boolean CF = lob == RoleLOB.CF;
        Boolean FAS = lob == RoleLOB.FAS;
        Boolean FR = lob == RoleLOB.FR;
        Boolean SC = lob == RoleLOB.SC;
        List<Staff_Role__c> roleList = Database.query('SELECT Name, Display_Name__c, Display_Order__c, FS__c, Registered_Only__c, Non_Registered_Only__c, Registered_Excluded_Job_Codes__c, Fin_Only__c, Non_Fin_Only__c ' +
                                                      'FROM Staff_Role__c ' +
                                 					  'WHERE ' + (opportunity ? 'Opportunity__c =: opportunity ' : 'Engagement__c =: engagement ') +
                                 					  'AND ' + (CF ? 'CF__c =: CF ' : FAS ? 'FAS__c =: FAS ' : FR ? 'FR__c =: FR ' : 'SC__c =: SC ') +
                                 					  'ORDER BY Display_Order__c');
        return roleList;
    }

    //Filters down the Selectable Staff Roles based on Custom Business Logic
    public static List<Staff_Role__c> FilterSelectableRoles(Contact c, String jobCode, Boolean bypassFS, Boolean isForeign, List<Staff_Role__c> roleList){
        Boolean isFS = c.Department == 'FS' || c.Department == 'FSCG';
        Set<String> stChecks = new Set<String> {'ADMIN','CORP','FIN',''}; //The rules are built around these staff types
        List<Staff_Role__c> finalList = roleList.clone();
        Staff_Role__c sr; //Current Role we are Looking At
        Integer offset = 0; //Because we are looping by index, when we remove, we have to take into account the new list size
        Boolean isRemoved;
        for(Integer i = 0; i<roleList.size(); i++){
            isRemoved = false;
            sr = roleList[i];
            //If staff member is in FS and Role is not available for FS, remove it
             if(isFS && !sr.FS__c && !bypassFS){
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }
            //If Staff Member is Registered (Domestic or Foreign) and Staff Type in ADMIN, CORP, FIN or BLANK, they cannot be in Non-Registered Roles (Intern for Examples)
            if(!isRemoved && c.Registered__c && (String.isBlank(c.Staff_Type__c) ||stChecks.contains(c.Staff_Type__c)) && sr.Non_Registered_Only__c){
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }
            //If Staff Member is Non Registered (Domestic) and the Deal is In the US
            if(!isRemoved && c.Staff_Type__c == 'FIN' && sr.Non_Fin_Only__c){//If staff member is FIN and role is Non-Fin Only
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }
            if(!isRemoved && c.Staff_Type__c != 'FIN' && sr.Fin_Only__c){//If staff member is Non-FIN and role is Fin Only
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }

            //If staff member is non-registered (U.S.) and deal is US based, they cannot be assigned to a registered only role (unless jobcode permits)
            if(!isRemoved && !c.Registered__c && !c.Is_Foreign_Office__c && !isForeign && sr.Registered_Only__c &&
                (String.IsBlank(sr.Registered_Excluded_Job_Codes__c) || String.IsBlank(jobCode) || !sr.Registered_Excluded_Job_Codes__c.contains(jobCode))){
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }

            //If staff member is non-registered(Foreign) and not FIN, they cannot be staffed
            //FIN foreign non-registered staff can be staffed on any U.S. deals
            if(!isRemoved && !c.Registered__c && c.Is_Foreign_Office__c && !isForeign && c.Staff_Type__c <> 'FIN' && sr.Registered_Only__c){
                finalList.remove(i-offset);
                offset++;
                isRemoved = true;
            }
        }

        return finalList;
    }

    public static List<Staff_Role__c> GetByName(String name){
        return [SELECT Name, Display_Name__c, Display_Order__c, FS__c,
                       Registered_Only__c, Non_Registered_Only__c, Registered_Excluded_Job_Codes__c, Fin_Only__c, Non_Fin_Only__c
                FROM Staff_Role__c
                WHERE Name =: name];
    }

    public static List<String> GetSortedRoles(Set<String> roles){
        List<String> results = new List<String>();
        for(Staff_Role__c sr : [SELECT Name
                                FROM Staff_Role__c
                                WHERE Name IN:roles ORDER BY Display_Order__c])
            results.add(sr.Name);

        return results;
    }
}