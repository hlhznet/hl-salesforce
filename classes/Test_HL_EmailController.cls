@isTest
private class Test_HL_EmailController {
    @isTest private static void TestEventEmail(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('',1,SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE)[0];
        c.User__c = UserInfo.getUserId();
        c.Office__c = 'LA';
        c.Email = 'sampletest@hl.com';
        insert c;
        Event e = SL_TestSetupUtils.CreateEvent('',1)[0];
        e.StartDateTime = DateTime.now();
        e.EndDateTime = e.StartDateTime;
        insert e;
        Email_Template__c et = new Email_Template__c(Name='Event', Distribution_Group__c = c.Id, Email_Subject__c = 'Test', Related_Object__c = 'Event', Template_Body__c = 'Test');
        insert et;
         
        //Setup the Query String Parameters
        ApexPages.currentPage().getParameters().put('template', et.Name);
        ApexPages.currentPage().getParameters().put('ro', e.Id);
        ApexPages.currentPage().getParameters().put('retURL', e.Id);
        
        Test.startTest();
        	HL_EmailController con = new HL_EmailController();
        try{con.SendEmail();}
        //Catch Email Exception if Mail is not Enabled
        catch(System.EmailException ex){}
        Test.stopTest();
        
        //Validate no page errors
		System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));
    }
    
     @isTest private static void TestOpportunityApprovalEmail(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('',1,SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE)[0];
        c.User__c = UserInfo.getUserId();
        c.Office__c = 'LA';
        c.Email = 'sampletest@hl.com';
        insert c;
        Opportunity__c o = SL_TestSetupUtils.CreateOpp('',1)[0];
        o.Name = 'Test Opp';
        insert o;
        Opportunity_Approval__c oa = SL_TestSetupUtils.CreateOA('',1)[0];
        oa.Related_Opportunity__c = o.Id;
        insert oa;
        Email_Template__c et = new Email_Template__c(Name='FEIS', Distribution_Group__c = c.Id, Email_Subject__c = 'Test', Related_Object__c = 'Opportunity_Approval__c', Template_Body__c = 'Test');
        insert et;
        
        //Setup the Query String Parameters
        ApexPages.currentPage().getParameters().put('template', et.Name);
        ApexPages.currentPage().getParameters().put('ro', oa.Id);
        ApexPages.currentPage().getParameters().put('retURL', oa.Id);
         
        Test.startTest();
        	HL_EmailController con = new HL_EmailController();
         	try{con.SendEmail();}
         	//Catch Email Exception if Mail is not Enabled
         	catch(Exception ex){}
        Test.stopTest();
         
        //Validate no page errors
		System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));
    }
}