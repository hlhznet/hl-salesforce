public class HL_OIT {
    //Returns True for Deal Team Members and Their Delegates
	public static Boolean IsActiveOnTeam(Id oppId, Id userId){
        Set<Id> supervisors = new Set<Id>();
        for(Delegate_Public_Group__c dpg : HL_DelegatePublicGroup.GetSupervisors())
            supervisors.add(dpg.Banker__c);
        //Add Current User to Make Query Easier
        supervisors.add(userId); 
        return ([SELECT Id FROM Opportunity_Internal_Team__c WHERE Opportunity__c =: oppId AND Contact__r.User__c IN: supervisors AND End_Date__c = null]).size() > 0;
    }
}