public with sharing class HL_OpportunityInternalTeamHandler {
    /* Start - Variable */
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    /* End - Variable */

    /* Start - Constructor */
    public HL_OpportunityInternalTeamHandler(boolean isExecuting, integer size) 
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    /* End - Constructor */

    /*
        @MethodName         : onAfterInsert
        @param              : List of Opportunity_Internal_Team__c
        @Description        : This function will be called on after insert of the Opportunity_Internal_Team__c records
    */
    public void onAfterInsert(List<Opportunity_Internal_Team__c> oitList)
    {
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.manageSharingRules(oitList, 'Opportunity__c', 'Opportunity_Internal_Team__c', 'insert'); 
        setupSharingforInsertedOITforRelatedOpportunity(oitList);                                                                                                           
    }
    /* End */
    
    /*
        @MethodName         : onAfterUpdate
        @param              : Old and new map of Opportunity_Internal_Team__c
        @Description        : This function will be called on after update of the Opportunity_Internal_Team__c records
    */
    public void onAfterUpdate(Map<Id, Opportunity_Internal_Team__c> mapOldOIT, Map<Id, Opportunity_Internal_Team__c> mapNewOIT, List<Opportunity_Internal_Team__c> oitList)
    {   
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.createDeleteShareOnUpdate(mapOldOIT, mapNewOIT, 'Opportunity__c', 'Opportunity_Internal_Team__c');
        if(HL_ConstantsUtil.IsSupervisorOrAdmin()) {
            UpdateOppVPPositionTeamMembers(oitList, mapOldOIT);
        }
        setupSharingforUpdatedOITforRelatedOpportunity(oitList, mapOldOIT);
    }
    /* End */
    
    /*
        @MethodName         : onAfterDelete
        @param              : map of Opportunity_Internal_Team__c
        @Description        : This function will be called on after delete of the Opportunity_Internal_Team__c records
                              to delete the Opportunity share records related to Opportunity_Internal_Team__c   
    */
    public void onAfterDelete(Map<Id, Opportunity_Internal_Team__c> mapOITOld, List<Opportunity_Internal_Team__c> oitList)
    {
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.manageSharingRules(mapOITOld.values(), 'Opportunity__c', 'Opportunity_Internal_Team__c', 'delete');
    }
    /* End */

    public static void UpdateEngagementTeamAssembled(List<Opportunity_Internal_Team__c> oitList){
        //Create Set of Opportunity Ids
        Set<Id> opps = new Set<Id>();
        List<String> engagementTeamRoles = new List<String> {'Principal','Seller','Manager','Initiator'};
        for(Opportunity_Internal_Team__c oit : oitList)
            opps.add(oit.Opportunity__c);
        
        //Get opportunities that may need to be updated
        List<Opportunity__c> possibleOpps = [SELECT Id, Engagement_Team_Assembled__c FROM Opportunity__c WHERE Id IN : opps];
        List<Opportunity_Internal_Team__c> existingEngagementRoles = [SELECT Opportunity__c, Staff_Role__r.Name FROM Opportunity_Internal_Team__c WHERE Opportunity__c IN : opps AND Staff_Role__r.Name IN : engagementTeamRoles AND (End_Date__c = null OR End_Date__c > : date.today())];
        //Create a Map of existing roles to validate against
        Map<String, Id> er = new Map<String,Id>();
        for(Opportunity_Internal_Team__c oit:existingEngagementRoles){
            if(er.get(oit.Opportunity__c + ':' + oit.Staff_Role__r.Name) == null)
                er.put(oit.Opportunity__c + ':' + oit.Staff_Role__r.Name, oit.Opportunity__c);
        }
        
        //Find which opps we need to update and then update
        List<Opportunity__c> oppsToUpdate = new List<Opportunity__c>();
        
        for(Opportunity__c opp : possibleOpps)
        {
            integer roleCount = 0;
            for(String r : engagementTeamRoles)
            {
                if(er.get(opp.Id + ':' + r) <> null)
                    roleCount++;
            }
            
            //Determine if opp needs to be updated
            if((opp.Engagement_Team_Assembled__c && roleCount <> engagementTeamRoles.size()) || (!opp.Engagement_Team_Assembled__c && roleCount >= engagementTeamRoles.size()))
            {
                 opp.Engagement_Team_Assembled__c = !opp.Engagement_Team_Assembled__c;
                 oppsToUpdate.add(opp);
            }
               
        }
        
        if(oppsToUpdate.size() > 0)
            update oppsToUpdate;

    }
    
    public static void UpdateInternalTeamAggregate(List<Opportunity_Internal_Team__c> oitList){
        //Oppt To Update
        List<Opportunity__c> oToUpdate = new List<Opportunity__c>();
        //Create Set of Opportunity Ids
        Set<Id> opps = new Set<Id>();
        for(Opportunity_Internal_Team__c oit : oitList)
            opps.add(oit.Opportunity__c);
        //Create a map of opportunity|role with team member ids
        Map<String,Set<String>> oppTeamMap = new Map<String,Set<String>>();
        for(Opportunity_Internal_Team__c o : [SELECT Opportunity__c, Contact__r.User__c, Staff_Role__r.Name FROM Opportunity_Internal_Team__c WHERE Opportunity__c IN : opps AND End_Date__c = null]){
                String key = o.Opportunity__c + '_' + o.Staff_Role__r.Name;
                if(oppTeamMap.get(key) == null)
                    oppTeamMap.put(key, new Set<String>{o.Contact__r.User__c});
                else
                {
                    Set<String> existingIds = (Set<String>)oppTeamMap.get(key);
                    existingIds.add(o.Contact__r.User__c);
                    oppTeamMap.put(o.Opportunity__c, existingIds);
                }
        }
        
        //Get opportunities that may need to be updated
        for(Opportunity__c o : [SELECT Id FROM Opportunity__c WHERE Id IN : opps])
        {
                o.z_Admin_Intern_Aggregate__c = PopulateAggregateField(o.Id + '_Intern', oppTeamMap);
                o.z_Analyst_Aggregate__c = PopulateAggregateField(o.Id + '_Analyst', oppTeamMap);
                if(o.z_Analyst_Aggregate__c.Length() > 255)
                {
                    o.z_Analyst_Aggregate2__c = o.z_Analyst_Aggregate__c.subString(247);
                    o.z_Analyst_Aggregate__c = o.z_Analyst_Aggregate__c.subString(0,246);
                }
                o.z_Associate_Aggregate__c = PopulateAggregateField(o.Id + '_Associate', oppTeamMap);
                if(o.z_Associate_Aggregate__c.Length() > 255){
                    o.z_Associate_Aggregate2__c = o.z_Associate_Aggregate__c.subString(247);
                    o.z_Associate_Aggregate__c = o.z_Associate_Aggregate__c.subString(0,246);
                }
                o.z_Final_Rev_Aggregate__c = PopulateAggregateField(o.Id + '_Final Reviewer', oppTeamMap);
                o.z_Initiator_Aggregate__c= PopulateAggregateField(o.Id + '_Initiator', oppTeamMap);
                if(o.z_Initiator_Aggregate__c.Length() > 255){
                    o.z_Initiator_Aggregate2__c = o.z_Initiator_Aggregate__c.subString(247);
                    o.z_Initiator_Aggregate__c = o.z_Initiator_Aggregate__c.subString(0,246);
                }
                o.z_Manager_Aggregate__c = PopulateAggregateField(o.Id + '_Manager', oppTeamMap);
                o.z_Marketing_Aggregate__c = PopulateAggregateField(o.Id + '_Marketing Team', oppTeamMap);
                if(o.z_Marketing_Aggregate__c.Length() > 255){
                    o.z_Marketing_Aggregate2__c = o.z_Marketing_Aggregate__c.subString(247);
                    if(o.z_Marketing_Aggregate2__c.Length() > 255){
                        o.z_Marketing_Aggregate3__c = o.z_Marketing_Aggregate2__c.subString(247);
                        o.z_Marketing_Aggregate2__c = o.z_Marketing_Aggregate2__c.subString(0,246);
                    }
                    o.z_Marketing_Aggregate__c = o.z_Marketing_Aggregate__c.subString(0,246);
                }
                o.z_NonRegistered_Aggregate__c = PopulateAggregateField(o.Id + '_Non-Registered', oppTeamMap);
                o.z_PE_HF_Aggregate__c = PopulateAggregateField(o.Id + '_PE/Hedge Fund', oppTeamMap);
                o.z_Prelim_Rev_Aggregate__c = PopulateAggregateField(o.Id + '_Prelim Reviewer', oppTeamMap);
                o.z_Pricing_Aggregate__c = PopulateAggregateField(o.Id + '_Pricing Committee Approver', oppTeamMap);
                o.z_Principal_Aggregate__c = PopulateAggregateField(o.Id + '_Principal', oppTeamMap);
                o.z_Public_Aggregate__c = PopulateAggregateField(o.Id + '_Public Person', oppTeamMap);
                o.z_Reviewer_Aggregate__c = PopulateAggregateField(o.Id + '_Reviewer', oppTeamMap);
                o.z_Seller_Aggregate__c = PopulateAggregateField(o.Id + '_Seller', oppTeamMap);
                o.z_Specialty_Aggregate__c = PopulateAggregateField(o.Id + '_Ind/Prod/Geog Person', oppTeamMap);
                oToUpdate.add(o);
        }
        if(oToUpdate.size() > 0)
            update oToUpdate;
    }

    private static String PopulateAggregateField(String mapKey, Map<String,Set<String>> teamMap){
        String fieldValue = '';
        if(teamMap.get(mapKey) <> null){
            for(String id : (Set<String>)teamMap.get(mapKey)){
                if(id <> null)
                    fieldValue = String.isBlank(fieldValue) ? id : (fieldValue.contains(id) ? fieldValue : fieldValue + '|' + id);
            }
        }
        else
            fieldValue = '';
        
        return fieldValue;
    }
    
    private static void UpdateOppVPPositionTeamMembers(List<Opportunity_Internal_Team__c> processedInternTeamMemberList , Map<Id,Opportunity_Internal_Team__c> processedInternTeamMemberMap){
        
        Set<Opp_VP_TeamMember__c> oppVPPosTeamMemberList = new Set<Opp_VP_TeamMember__c>();
        List<Opp_VP_TeamMember__c> oppVPPosTeamMemberToDeleteList = new List<Opp_VP_TeamMember__c>();
        
        Set<Id> relatedOpportunityIdSet = new Set<Id>();
        Set<Id> relatedContactIdSet = new Set<Id>();
        Map<Id, date> contactWithRecentEndDateMap = new Map<Id, date>(); 
        for(Opportunity_Internal_Team__c processedInternTeamMember : processedInternTeamMemberList) {
            if(processedInternTeamMember.End_Date__c != null && processedInternTeamMember.End_Date__c != processedInternTeamMemberMap.get(processedInternTeamMember.Id).End_Date__c)
                relatedOpportunityIdSet.add(processedInternTeamMember.Opportunity__c);  
                relatedContactIdSet.add(processedInternTeamMember.Contact__c);
        }
        
        Set<Id> ContactsToExclude = new Set<Id>();
        // we will exlucde only that contact which has at least one opportunity team member record with blank end date ( it means opp team memeber still is in the list under opportunity) 
        for(Contact cont : [SELECT Id, (SELECT Id,End_Date__c FROM Opportunity_Teams__r WHERE End_Date__c = null AND Opportunity__c In : relatedOpportunityIdSet) 
                            FROM Contact 
                            WHERE Id In : relatedContactIdSet]) {
            If(cont.Opportunity_Teams__r.size() > 0)
                ContactsToExclude.add(cont.Id);
        }               
        // Purpose of excluding such contact is to avoid end date populating on Opp VP Team member record where opp Team member still under opportunity.  
        relatedContactIdSet.removeAll(ContactsToExclude);       
        
        // To calculate most recent end date to be populated in to Opp VP Team member
        AggregateResult[] groupedResults = [SELECT Contact__c contactId, MAX(End_Date__c) recentEndDate  
                                            FROM Opportunity_Internal_Team__c 
                                            WHERE End_Date__c != null AND Opportunity__c IN : relatedOpportunityIdSet AND Contact__c IN : relatedContactIdSet GROUP BY Contact__c ];
        for (AggregateResult ar : groupedResults) {
            contactWithRecentEndDateMap.put(String.valueof(ar.get('contactId')), Date.valueof(ar.get('recentEndDate')));
        }
        
        for(Opp_VP_TeamMember__c oppVPTM : [SELECT Id, Start_date__c, End_Date__c, Staff__c 
                                            FROM Opp_VP_TeamMember__c 
                                            WHERE Opportunity_VP_Position__r.Status__c != 'Cancelled' 
                                            AND Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__r.Converted_to_Engagement__c = false  
                                            AND (Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__r.Approval_Process__c != :label.HL_AppProcessForEngNo  OR ( Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__r.Approval_Process__c = :label.HL_AppProcessForEngNo AND Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__r.Engagement_Approval_Status__c != :label.HL_Pending_EngReqNo  ))     
                                            AND ResultingEngagemnetRTonVPTM__c = 'Portfolio_Valuation'                                                               
                                            AND Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__c IN : relatedOpportunityIdSet AND Staff__c IN : relatedContactIdSet ]) {
            
            if(oppVPTM.End_Date__c == null || oppVPTM.End_Date__c >= contactWithRecentEndDateMap.get(oppVPTM.Staff__c)) 
                oppVPTM.End_Date__c = contactWithRecentEndDateMap.get(oppVPTM.Staff__c);
            if(contactWithRecentEndDateMap.get(oppVPTM.Staff__c) != null) {
                if(oppVPTM.Start_date__c != null && oppVPTM.Start_date__c > oppVPTM.End_Date__c ) {
                    oppVPPosTeamMemberToDeleteList.add(oppVPTM);
                }
                else {
                    oppVPTM.End_Date__c = contactWithRecentEndDateMap.get(oppVPTM.Staff__c);
                    oppVPTM.Is_Team_Member_Deactivated_Once__c = true;
                    oppVPPosTeamMemberList.add(oppVPTM);                    
                }
            }
            
        }
        
        if(oppVPPosTeamMemberToDeleteList.size() > 0){            
            delete oppVPPosTeamMemberToDeleteList;    
        }    
        if(oppVPPosTeamMemberList.size() > 0){
            Set<Opp_VP_TeamMember__c> setofRecordsToBeRemoved = new Set<Opp_VP_TeamMember__c>();
            setofRecordsToBeRemoved = HL_ContactHandler.removeDuplicateOppVpTMRecrds(oppVPPosTeamMemberList);             
            oppVPPosTeamMemberList.removeAll(setofRecordsToBeRemoved);              
            List<Opp_VP_TeamMember__c> ListOfVPTMs = new List<Opp_VP_TeamMember__c>();
            ListOfVPTMs.addAll(oppVPPosTeamMemberList);            
            update ListOfVPTMs;     
        }
    }
    
    private static list<Opp_VP__c> OppVPList;
    // Commented code by Sandeep SF-820
    //private static list<Opp_VP_Position__c> OppVPPositionsList;
    //private static list<Opp_VP_TeamMember__c> OppVPTeamMemberList;
    
    private static void setupSharingforUpdatedOITforRelatedOpportunity(list<Opportunity_Internal_Team__c> oitList, map<Id, Opportunity_Internal_Team__c> mapOldOIT) {
        
        list<Opportunity_Internal_Team__c> ValidOITList = new list<Opportunity_Internal_Team__c>();
        Set<Id> RelatedOpportunityIdSet = new Set<Id>();
        Set<String> OppContactCombinationSetforsiblingOITs = new Set<String>();
        list<Opportunity_Internal_Team__c> SiblingOITs = new list<Opportunity_Internal_Team__c>();
        for(Opportunity_Internal_Team__c oit : [SELECT Id, Contact__r.User__c, End_Date__c, Opportunity__c  
                                                FROM Opportunity_Internal_Team__c
                                                WHERE Id IN :oitList ])
        { 
            if(oit.End_Date__c != Null && oit.End_Date__c != mapOldOIT.get(oit.id).End_Date__c)
            {
                ValidOITList.add(oit);              
                RelatedOpportunityIdSet.add(oit.Opportunity__c);
            }
        }
        
        for(Opportunity_Internal_Team__c siblingOIT : [SELECT Id , Opportunity__c, Contact__c, contact__r.user__c
                                                      FROM Opportunity_Internal_Team__c
                                                      WHERE End_Date__c = NULL AND Opportunity__c IN :RelatedOpportunityIdSet ])
        {
            OppContactCombinationSetforsiblingOITs.add(siblingOIT.Opportunity__c+'_'+siblingOIT.Contact__c);
            SiblingOITs.add(siblingOIT);
        } 
        
        list<Opportunity_Internal_Team__c> filtered_Level1_OITList = new list<Opportunity_Internal_Team__c>();
        Set<Opportunity_Internal_Team__c> filtered_Level2_OITList = new Set<Opportunity_Internal_Team__c>();
        for(Opportunity_Internal_Team__c  validOIT : ValidOITList){ 
            if(!OppContactCombinationSetforsiblingOITs.contains(validOIT.Opportunity__c+'_'+validOIT.Contact__c))
                filtered_Level1_OITList.add(validOIT);
            else 
                SiblingOITs.add(validOIT);
        }
        
        map<String, list<Id>> OpportunityWithRelatedTMDelegatedUserMap = new map<String, list<Id>>();
        map<String, list<Id>> OpportunityWithRelatedTMDelegatedToValidUserMap = new map<String, list<Id>>();
        
        if(filtered_Level1_OITList.size() > 0)
            OpportunityWithRelatedTMDelegatedToValidUserMap = HL_Opp_VP_TriggerHelper.fetchUserGroupOnlyRelatedToOIT(filtered_Level1_OITList);     
        
        if(SiblingOITs.size() > 0)
            OpportunityWithRelatedTMDelegatedUserMap = HL_Opp_VP_TriggerHelper.fetchUserGroupOnlyRelatedToOIT(SiblingOITs);
            
            
        Set<Id> tempSet = new Set<Id>();
        Boolean isfound = false;
        Set<Id> StopDeletionSharing = new Set<Id>();
        for(Opportunity_Internal_Team__c filtered_OIT : filtered_Level1_OITList)
        {
            isfound = false;
            if(OpportunityWithRelatedTMDelegatedUserMap.containsKey(filtered_OIT.Opportunity__c +'_TM')) 
            {
                tempSet = new Set<Id>();
                tempSet.addAll(OpportunityWithRelatedTMDelegatedUserMap.get(filtered_OIT.Opportunity__c +'_TM'));
                if(tempSet.contains(filtered_OIT.Contact__r.User__c))
                    isfound = true;                 
            }            
            if(OpportunityWithRelatedTMDelegatedUserMap.containsKey(filtered_OIT.Opportunity__c +'_DU'))                
            {
                tempSet = new Set<Id>();
                tempSet.addAll(OpportunityWithRelatedTMDelegatedUserMap.get(filtered_OIT.Opportunity__c +'_DU'));
                if(tempSet.contains(filtered_OIT.Contact__r.User__c))
                    isfound = true;
                if(OpportunityWithRelatedTMDelegatedToValidUserMap.containsKey(filtered_OIT.Opportunity__c +'_DU')) 
                {
                    for(Id UorGpId : OpportunityWithRelatedTMDelegatedToValidUserMap.get(filtered_OIT.Opportunity__c +'_DU')) {
                        if(tempSet.contains(UorGpId))
                        {
                            StopDeletionSharing.add(UorGpId);
                        }
                    }
                    
                }
            }            
            if(!isfound)
                filtered_Level2_OITList.add(filtered_OIT);
        }
        
        Set<Id> finalUserIdSetToRemoveSharing = new Set<Id>();
        
        for(Opportunity_Internal_Team__c filtered_OIT2 : filtered_Level2_OITList)
        {
            finalUserIdSetToRemoveSharing.add(filtered_OIT2.Contact__r.User__c);
        }
        
        fetchingVPFamily(RelatedOpportunityIdSet);

        list<Opp_VP__Share> listSharedVpToBeDelete = new list<Opp_VP__Share>();
            listSharedVpToBeDelete = [Select Id from Opp_VP__Share Where ParentId =: OppVPList AND UserOrGroupId IN: finalUserIdSetToRemoveSharing AND UserOrGroupId NOT IN : StopDeletionSharing];
        // commented code by Sandeep SF-820
        /*list<Opp_VP_Position__share> listSharedVpPositionToDelete = new list<Opp_VP_Position__share>();
            listSharedVpPositionToDelete = [Select Id from Opp_VP_Position__share Where ParentId =: OppVPPositionsList AND UserOrGroupId IN: finalUserIdSetToRemoveSharing AND UserOrGroupId NOT IN : StopDeletionSharing];
        list<Opp_VP_TeamMember__share> listSharedVpTMToBeDeleted = new list<Opp_VP_TeamMember__share>();
            listSharedVpTMToBeDeleted = [Select Id from Opp_VP_TeamMember__share Where ParentId =: OppVPTeamMemberList AND UserOrGroupId IN: finalUserIdSetToRemoveSharing AND UserOrGroupId NOT IN : StopDeletionSharing];
        */
        if(listSharedVpToBeDelete.size() >0 )   
            delete listSharedVpToBeDelete;
        // commented code by Sandeep SF-820
        /*if(listSharedVpPositionToDelete.size() >0 ) 
            delete listSharedVpPositionToDelete; 
        if(listSharedVpTMToBeDeleted.size() >0 )    
            delete listSharedVpTMToBeDeleted;
        */  
    }    
   
    private static void setupSharingforInsertedOITforRelatedOpportunity(list<Opportunity_Internal_Team__c> oitList) {
        
        map<String, list<Id>> OpportunityWithRelatedTMDelegatedUserMap = new map<String, list<Id>>();       
        list<Opportunity_Internal_Team__c> OppInternalTeamMemberList = new list<Opportunity_Internal_Team__c>();
        Set<Id> reltedOpportunityIdSet = new Set<Id>();
        for(Opportunity_Internal_Team__c OppInternalTeamMember : [SELECT contact__r.user__c, Opportunity__c 
                                                                  FROM Opportunity_Internal_Team__c 
                                                                  WHERE End_Date__c = NULL AND Id IN : oitList])
        {
            OppInternalTeamMemberList.add(OppInternalTeamMember);
            reltedOpportunityIdSet.add(OppInternalTeamMember.Opportunity__c);
                                                                            
        }
            
        OpportunityWithRelatedTMDelegatedUserMap = HL_Opp_VP_TriggerHelper.fetchUserGroupOnlyRelatedToOIT(OppInternalTeamMemberList);
        
        fetchingVPFamily(reltedOpportunityIdSet);
       
        HL_Opp_VP_TriggerHelper.SetupSharingCoreLogicForOppVP(OppVPList, OpportunityWithRelatedTMDelegatedUserMap);
        // commented Code by Sandeep SF-820
        //HL_Opp_VP_PositionTriggerHelper.SetupSharingCoreLogicForOppVPPosition(OppVPPositionsList, OpportunityWithRelatedTMDelegatedUserMap);
        //HL_Opp_VP_TeamMemberTriggerHelper.SetupSharingCoreLogicForOppVPTeamMembers(OppVPTeamMemberList, OpportunityWithRelatedTMDelegatedUserMap);
        
    }
    
    public static void fetchingVPFamily(set<Id> reltedOpportunityIdSet) {
        OppVPList = new list<Opp_VP__c>();
        // commented Code by Sandeep SF-820
        //OppVPPositionsList = new list<Opp_VP_Position__c>();
        //OppVPTeamMemberList = new list<Opp_VP_TeamMember__c>();
         //Updated query by Sandeep SF-820
        /*for(Opp_VP__c OppVP : [SELECT Id, Opportunity__c,ownerId, (SELECT Id, Opportunity_VP__r.Opportunity__c,ownerId  FROM Opp_VP_Positions__r)
                               FROM Opp_VP__c
                               WHERE Opportunity__c IN : reltedOpportunityIdSet])*/
        for(Opp_VP__c OppVP : [SELECT Id, Opportunity__c,ownerId 
                               FROM Opp_VP__c
                               WHERE Opportunity__c IN : reltedOpportunityIdSet])
        {
            OppVPList.add(OppVP);
            // Commented Code by Sandeep SF-820
            //OppVPPositionsList.addAll(OppVP.Opp_VP_Positions__r);           
        }
        // Commented Code by Sandeep SF-820
        /*OppVPTeamMemberList = [SELECT id, Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__c,ownerId  
                               FROM Opp_VP_TeamMember__c 
                               WHERE Opportunity_VP_Position__r.Opportunity_VP__r.Opportunity__c IN : reltedOpportunityIdSet];*/
    }   
}