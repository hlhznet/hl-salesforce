public class HL_RevenueProjectionHandler {
    public void onBeforeInsert(List<Revenue_Projection__c> newRevenueProjectionList){
    	Set<Id> engId = new Set<Id>();
    	Map<Id,Engagement__c> engagementMap = new Map<Id,Engagement__c>();
    	for(Revenue_Projection__c rp : newRevenueProjectionList){
    		engId.add(rp.Engagement__c);
    	}
    	for(Engagement__c eng : [SELECT CurrencyIsoCode FROM Engagement__c WHERE Id IN : engId]){
    		engagementMap.put(eng.Id, eng);
    	}
        updateCurrencyIso(engagementMap, newRevenueProjectionList);
		updateArchived(newRevenueProjectionList);
    }

	public void onBeforeUpdate(List<Revenue_Projection__c> newRevenueProjectionList){
        updateArchived(newRevenueProjectionList);
    }

	//Manage Revenue Revenue Projection Sharing Rules
    public void onAfterInsert(Map<Id, Revenue_Projection__c> newRevenueProjectionMap){
        SL_ManageSharingRules.manageRevAccrualsCounterpartySharingRules(newRevenueProjectionMap.values(), 'Revenue_Projection__c');
    }

    public void updateCurrencyIso(Map<Id,Engagement__c> engagementMap, List<Revenue_Projection__c> revenueProjectionList) {
    	for(Revenue_Projection__c rp : revenueProjectionList){
    		rp.CurrencyIsoCode = engagementMap.get(rp.Engagement__c).CurrencyIsoCode;
    	}
    }

	public void updateArchived(List<Revenue_Projection__c> newRevenueProjectionList) {
		integer year = System.Today().year();
        integer month = System.Today().month();

        for(Revenue_Projection__c p: newRevenueProjectionList) {
            if (Integer.valueof(p.Month__c.trim()) < month && Integer.valueof(p.Year__c.trim()) <= year) {
                p.Archive__c = true;
            }
        }   
	}
}