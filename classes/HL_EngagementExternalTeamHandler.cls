public class HL_EngagementExternalTeamHandler {
    private List<Engagement_External_Team__c> EngagementExternalTeam {get; set;}

    public HL_EngagementExternalTeamHandler(List<Engagement_External_Team__c> engagementExternalTeam){
        this.EngagementExternalTeam = engagementExternalTeam;
    }
    //Populate The EET_UNIQUE__c Field
    public List<Engagement_External_Team__c> UpdateEETUnique(){
        for(Engagement_External_Team__c eet : EngagementExternalTeam)
                eet.EET_UNIQUE__c = eet.Engagement__c + '|' + eet.Contact__c;
        
        return EngagementExternalTeam;
    }
    
    //Updates the Primary External Contact Lookup field on the Engagements
    //Also updates the Primary Checkbox on other EET fields to make sure only one is marked as primary per Engagement
    public List<Engagement_External_Team__c> UpdatePrimaryEngagementContact(){
        Map<Id,Id> engsToPrimaryEET = new Map<Id,Id>(); //Map of Engagements to Primary
        List<Engagement__c> engsToUpdate = new List<Engagement__c>();
        List<Engagement_External_Team__c> eetsToUpdate = new List<Engagement_External_Team__c>();
        Set<Id> eets = new Set<Id>();
        Id primaryContact;
        for(Engagement_External_Team__c eet: EngagementExternalTeam){
            if(eet.Primary__c){
                eets.add(eet.Id);
                engsToPrimaryEET.put(eet.Engagement__c, eet.Contact__c);
            }
        }
        //Update the Primary External Contact Lookup Field on the Engagement
        if(engsToPrimaryEET.size() > 0){
            for(Engagement__c e : [SELECT Primary_External_Contact__c FROM Engagement__c WHERE Id IN:engsToPrimaryEET.keySet()]){
               	primaryContact = engsToPrimaryEET.get(e.Id);
                if(e.Primary_External_Contact__c <> primaryContact){
                    e.Primary_External_Contact__c = primaryContact;
                    engsToUpdate.add(e);
                }
            }
        }
        //Update the Engagements that might have Changed
        if(engsToUpdate.size() > 0){
            update engsToUpdate;
            
            for(Engagement_External_Team__c eet : [SELECT Primary__c FROM Engagement_External_Team__c WHERE Id NOT IN:eets AND Engagement__c IN:engsToPrimaryEET.keySet() AND Primary__c = true]){
                eet.Primary__c = false;
                eetsToUpdate.add(eet);
            }
            
            if(eetsToUpdate.size() > 0)
                update eetsToUpdate;
        }
        
        return EngagementExternalTeam;
    }
    
    //Handle the Before Delete to Remove the Primary Reference if the Contact is Deleted
    public List<Engagement_External_Team__c> RemovePrimaryEET(){
        Set<Id> engIds = new Set<Id>(); //Store the Engagements to Update
        List<Engagement__c> engsToUpdate = new List<Engagement__c>();
        for(Engagement_External_Team__c eet: EngagementExternalTeam){
            if(eet.Primary__c)
                engIds.add(eet.Engagement__c);
        }
        
        for(Engagement__c e : [SELECT Primary_External_Contact__c FROM Engagement__c WHERE Id IN:engIds]){
            e.Primary_External_Contact__c = null;
            engsToUpdate.add(e);
        }
        
        if(engsToUpdate.size() > 0)
            update engsToUpdate; 
        
        return EngagementExternalTeam;
    }
    
    public List<Engagement_External_Team__c> UpdateSortFields(){
        Set<Id> engIds = new Set<Id>();
        Set<String> roles = new Set<String>();
        Set<Id> contacts = new Set<Id>();
        Id engId, acctId;
        String companySort, role, roleSort;
        
        for(Engagement_External_Team__c eet : EngagementExternalTeam){
            engIds.add(eet.Engagement__c);
            roles.add(eet.Role__c);
            contacts.add(eet.Contact__c);
        }
		
        //Get a contact map for the account Ids
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT AccountId FROM Contact WHERE Id IN:contacts]);
        
        for(AggregateResult ar : [SELECT Engagement__c, Role__c, Contact__r.AccountId accountId, MAX(Sort_Role_Order__c) sortRole, MAX(Sort_Company_Order__c) sortCompany FROM Engagement_External_Team__c WHERE Engagement__c IN: engIds AND Role__c IN: roles GROUP BY Engagement__c, Role__c, Contact__r.AccountId]){
            engId = (Id)ar.get('Engagement__c');
            acctId = (Id)ar.get('accountId');
            role = (String)ar.get('Role__c');
            roleSort = (String)ar.get('sortRole');
            companySort = (String)ar.get('sortCompany');
            for(Engagement_External_Team__c eet : EngagementExternalTeam){
                if(eet.Engagement__c == engId && eet.Role__c == role){
                    eet.Sort_Role_Order__c = roleSort;
                    if(contactMap.get(eet.Contact__c).AccountId == acctId)
                        eet.Sort_Company_Order__c = companySort;
                }
            }
        }
        
        return EngagementExternalTeam;
    }
}