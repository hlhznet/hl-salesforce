@isTest
private class Test_HL_EngagementHandler {

    private static testmethod void TestFAS_StageUpdates(){
        Account clientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert clientAccount;

        Account subjectAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert subjectAccount;

        //FAS Engagements
        List<Engagement__c> eFAS_List = SL_TestSetupUtils.CreateEngagement('', 2);
        for(Engagement__c e : eFAS_List){
            e.Name='Test FAS Engagement';
            e.Client__c = clientAccount.Id;
            e.Subject__c = subjectAccount.Id;
            e.Line_of_Business__c = 'FAS';
            e.Job_Type__c = 'FMV Transaction Based Opinion';
            e.Primary_Office__c = 'NY';
            e.Total_Estimated_Fee__c = 100000;
            e.RecordTypeId = '012i0000001NDwh';
        }
        //Create one with 0% completed to Update to 50%
        Engagement__c eFAS_Stage50 = eFAS_List[0];
        eFAS_Stage50.Stage__c = 'Retained';

        //Create one with 75% completed to Update to 100%
        Engagement__c eFAS_Stage100 = eFAS_List[1];
        eFAS_Stage100.Stage__c = 'Opinion Report';

        insert eFAS_Stage50;
        insert eFAS_Stage100;

        Test.startTest();

        //Change the Stage to 50% Complete
        eFAS_Stage50.Stage__c = 'Performing Analysis';
        update eFAS_Stage50;

        //Change the Stage to 100% Complete
        eFAS_Stage100.Final_Report_Sent_Date__c = Date.today();
        eFAS_Stage100.Stage__c = 'Bill/File';
        update eFAS_Stage100;

        Test.stopTest();

        //Validate the Period Accrued Fees were updated appropriately
        eFAS_Stage50 = [SELECT Total_Estimated_Fee__c, Period_Accrued_Fees__c FROM Engagement__c WHERE Id=:eFAS_Stage50.Id];
        System.assertEquals(eFAS_Stage50.Total_Estimated_Fee__c / 2, eFAS_Stage50.Period_Accrued_Fees__c);
        eFAS_Stage100 = [SELECT Total_Estimated_Fee__c, Period_Accrued_Fees__c FROM Engagement__c WHERE Id=:eFAS_Stage100.Id];
        System.assertEquals(eFAS_Stage100.Total_Estimated_Fee__c, eFAS_Stage100.Period_Accrued_Fees__c);
    }

    private static testmethod void TestOnBeforeInsert(){
        Engagement__c e = SL_TestSetupUtils.CreateEngagement('',1)[0];

        Test.startTest();

        HL_EngagementHandler handler = new HL_EngagementHandler(false, 1);
        insert e;

        Test.stopTest();

        System.assert(!String.isBlank(e.Id));
    }

    private static testmethod void TestOnBeforeUpdate(){
        Exchange_Rate__c ex = new Exchange_Rate__c(CurrencyIsoCode = 'AUD');
        insert ex;
        Engagement__c e = SL_TestSetupUtils.CreateEngagement('',1)[0];
        insert e;
        List<Engagement__c> oldEngagementList = [SELECT CurrencyIsoCode, Engagement_Comment__c, Job_Type__c FROM Engagement__c WHERE Id =: e.Id];
        Map<Id, Engagement__c> oldEngagementMap = new Map<Id, Engagement__c>();
        oldEngagementMap.put(oldEngagementList[0].Id, oldEngagementList[0]);
        
        Engagement__c newEng = e.Clone();
        newEng.Engagement_Comment__c = 'Test Comment';
        newEng.CurrencyIsoCode = 'AUD';
        newEng.Engagement_Number__c = '123456';
        insert newEng;
        List<Engagement__c> newEngagementList = new List<Engagement__c>{newEng};
        Map<Id, Engagement__c> newEngagementMap = new Map<Id, Engagement__c>();
        newEngagementMap.put(e.Id, newEng);

        Test.startTest();

        HL_EngagementHandler handler = new HL_EngagementHandler(false, 1);
        handler.OnBeforeUpdate(newEngagementList, newEngagementMap, oldEngagementList, oldEngagementMap);

        Test.stopTest();

        System.assertEquals(handler.IsTriggerContext,false);
    }

    private static testmethod void TestEngagementJoinerCreations()
    {
        Account objClientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert objClientAccount;

        Account objClientAccountToUpdate = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert objClientAccountToUpdate;

        Account objSubjectAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert objSubjectAccount;

        List<Opportunity__c> opportunityList = new List<Opportunity__c>();
        for(Opportunity__c objOpportunity : SL_TestSetupUtils.CreateOpp('Opportunity__c', 1))
        {
            objOpportunity.Estimated_Capital_Raised_MM__c = 123456789;
            objOpportunity.Job_Type__c = 'Board Advisory Services (BAS)';
            opportunityList.add(objOpportunity);
        }
        insert opportunityList;

        Engagement__c objEngagementNew = new Engagement__c(Name='Test Engagement', Engagement_Number__c = '123456', Client__c = objClientAccount.Id, Opportunity__c = opportunityList[0].Id,
                                                              Subject__c = objSubjectAccount.Id, Stage__c = 'Advisory',
                                                              Line_of_Business__c = 'CF', Job_Type__c = 'Take Over Defense',
                                                              Primary_Office__c = 'AT', Industry_Group__c = 'BUS');

        Monthly_Revenue_Process_Control__c mrpcNew = new Monthly_Revenue_Process_Control__c(IsCurrent__c = true,
                                                              Current_Month__c = '02', Current_Year__c = '2015');
        insert mrpcNew;

        Test.startTest();

        insert objEngagementNew;

        Engagement_Counterparty__c objEC = new Engagement_Counterparty__c(Name = 'TestEC', Company__c = objSubjectAccount.Id,
                                                                      Engagement__c = objEngagementNew.Id);
        insert objEC;

        Revenue_Accrual__c objRA = new Revenue_Accrual__c(Engagement__c = objEngagementNew.Id, Month__c = 'Jan', Year__c = '2003');
        insert objRA;

        List<Engagement_Client_Subject__c> ecsAfterInsertResultList = [SELECT Id FROM Engagement_Client_Subject__c WHERE Engagement__c = :objEngagementNew.Id];

        objEngagementNew.Name = 'dndTestEngagement';
        objEngagementNew.Client__c = objClientAccountToUpdate.Id;
        update objEngagementNew;

        Test.stopTest();

        //check whether client subject records are created  - they shouldn't be because of our conversion process
        System.AssertEquals(ecsAfterInsertResultList.Size(), 0);

        // check whether the previous combination of joiner record is deleted, Now it should be deleted
        System.AssertEquals([SELECT Id FROM Engagement_Client_Subject__c WHERE Engagement__c = :objEngagementNew.Id AND Client_Subject__c = :objClientAccount.Id].Size(), 0);
        // check whether the updated client Account is created in the Joiner
        System.AssertEquals([SELECT Id FROM Engagement_Client_Subject__c WHERE Engagement__c = :objEngagementNew.Id].Size(), 1);
    }


    private static testmethod void TestEngagementFeeNewUpdate(){
                Account objClientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert objClientAccount;
        //PLEASE REMOVE THIS OPPTY INSERTION IF NOT NEEDED
        List<Opportunity__c> opportunityList = new List<Opportunity__c>();
        for(Opportunity__c objOpportunity : SL_TestSetupUtils.CreateOpp('Opportunity__c', 1))
        {
            objOpportunity.Estimated_Capital_Raised_MM__c = 123456789;
            objOpportunity.Job_Type__c = 'Board Advisory Services (BAS)';
            opportunityList.add(objOpportunity);
        }
        insert opportunityList;
        
         Engagement__c objEngagementNew = new Engagement__c(Name='Test Engagement', Engagement_Number__c = '123456', Client__c = objClientAccount.Id, Opportunity__c = opportunityList[0].Id,
                                                              Subject__c = objClientAccount.Id, Stage__c = 'Advisory',
                                                              Line_of_Business__c = 'CF', Job_Type__c = 'Take Over Defense',
                                                              Primary_Office__c = 'AT', Industry_Group__c = 'BUS',Total_Estimated_Fee__c=10.00);
                                                                          
        insert objEngagementNew;
        
        Eng_VP__c EngVPList =(Eng_VP__c)HL_TestFactory.CreateSObject('Eng_VP__c',false);        
        EngVPList.Name='Test Eng Vp';        
        EngVPList.Engagement__c=objEngagementNew.id;
        EngVPList.Valuation_Date__c=System.Today();
        EngVPList.CurrencyIsoCode='USD';   
        EngVPList.Fee_Total__c = 10.00;     
        Insert EngVPList;
        
        test.startTest();
            EngVPList.Fee_Total__c = 1000.00;
            update EngVPList;
            objEngagementNew.Total_Estimated_Fee__c = 100.00;
            update objEngagementNew;
        test.stopTest();
        System.AssertEquals(EngVPList.Fee_Total__c , 1000.00);
        System.AssertNotEquals(objEngagementNew.Total_Estimated_Fee__c , 20000.10);
        System.AssertEquals(objEngagementNew.Total_Estimated_Fee__c , 100.00);
        System.AssertNotEquals(objEngagementNew.Total_Estimated_Fee__c ,200.10);
    }

    private static testmethod void TestEngagementFeeUpdate(){
        Account objClientAccount = SL_TestSetupUtils.CreateAccount('', 1)[0];
        insert objClientAccount;
        
        List<Opportunity__c> opportunityList = new List<Opportunity__c>();
        for(Opportunity__c objOpportunity : SL_TestSetupUtils.CreateOpp('Opportunity__c', 1))
        {
            objOpportunity.Estimated_Capital_Raised_MM__c = 123456789;
            objOpportunity.Job_Type__c = 'Board Advisory Services (BAS)';
            opportunityList.add(objOpportunity);
        }
        insert opportunityList;
        
         Engagement__c objEngagementNew = new Engagement__c(Name='Test Engagement', Engagement_Number__c = '123456', Client__c = objClientAccount.Id, Opportunity__c = opportunityList[0].Id,
                                                              Subject__c = objClientAccount.Id, Stage__c = 'Advisory',
                                                              Line_of_Business__c = 'CF', Job_Type__c = 'Take Over Defense',
                                                              Primary_Office__c = 'AT', Industry_Group__c = 'BUS',Total_Estimated_Fee__c=10.00);
                                                                          
        insert objEngagementNew;
        
        test.startTest();
            objEngagementNew.Total_Estimated_Fee__c = 100.00;
            update objEngagementNew;
            
        test.stopTest();
        System.AssertEquals(objEngagementNew.Total_Estimated_Fee__c ,100.00);
        System.AssertNotEquals(objEngagementNew.Total_Estimated_Fee__c ,100.10);
    }
}