@isTest
private class Test_HL_EngagementExternalTeamHandler {
	@isTest private static void TestBasicFunctionality(){
        Contact c = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        insert c;
        Engagement__c e = HL_TestFactory.CreateEngagement(false);
        insert e;
        Engagement_External_Team__c eet = (Engagement_External_Team__c)HL_TestFactory.CreateSObject('Engagement_External_Team__c', false);
        eet.Contact__c = c.Id;
        eet.Engagement__c = e.Id;
        eet.Primary__c = true;

        Test.startTest();

        insert eet;

        Test.stopTest();

        //Verify records were created
        System.assert([SELECT Id FROM Engagement_External_Team__c WHERE Engagement__c =: e.Id].size() > 0);
    }

     @isTest private static void TestRemovePrimaryEET(){
        Contact c = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        insert c;
        Engagement__c e = HL_TestFactory.CreateEngagement(false);
        insert e;
        Engagement_External_Team__c eet = (Engagement_External_Team__c)HL_TestFactory.CreateSObject('Engagement_External_Team__c', false);
        eet.Contact__c = c.Id;
        eet.Engagement__c = e.Id;
        eet.Primary__c = true;

        Test.startTest();

        insert eet;
        HL_EngagementExternalTeamHandler handler = new HL_EngagementExternalTeamHandler(new List<Engagement_External_Team__c>{eet});
        handler.RemovePrimaryEET();

        Test.stopTest();

        e = [SELECT Primary_External_Contact__c FROM Engagement__c WHERE Id =: e.Id];
        //Verify the Primary Contact Field has been Blanked Out
        System.assert(String.isBlank(e.Primary_External_Contact__c));
    }
}