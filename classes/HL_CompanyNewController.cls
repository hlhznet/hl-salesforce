public class HL_CompanyNewController {
  
    public boolean hasDuplicateResult{get;set;}
    public String errorDuplicateText{get;set;}
    public Account NewAccount {get;set;}
    
    public List<Account> duplicateRecords;        
    public ApexPages.StandardController Con {get; set;}
    
    public HL_CompanyNewController(ApexPages.StandardController controller){
        if(controller.getRecord() <> null)
        {            
            NewAccount = (Account)controller.getRecord();
        }
            
        Con = controller;
       // Con.addFields(new List<String>{'BillingCountry'});
        //Con.addFields(new List<String>{'BillingState'});
        this.duplicateRecords = new List<Account>();
        this.hasDuplicateResult = false;        
    }
     
    
    public List<Account> getDuplicateRecords() {
    
         system.debug('Data in duplicateRecords' + duplicateRecords);
        return this.duplicateRecords;
    }
        
    public PageReference SaveAndIgnore(){
       try{
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true;        
            NewAccount = (Account)Con.getRecord();                        
            Database.SaveResult sr2 = Database.insert(NewAccount, dml);
            return (new ApexPages.StandardController(NewAccount)).view();
        }catch(DmlException ex){            
             HL_PageUtility.ShowError(ex);
             return null;
         }
    
        return null;
    }
    
     public PageReference saveAndNew() {        
         try{
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true;        
            NewAccount = (Account)Con.getRecord();                        
            Database.SaveResult sr2 = Database.insert(NewAccount, dml);
            string s = '/' + ('' + NewAccount.get('Id')).subString(0, 3) + '/e?';            
            return new Pagereference(s);             
        }catch(DmlException ex){            
             HL_PageUtility.ShowError(ex);
             return null;
         }
    
        return null;
    }

    
    public PageReference SaveRecord() {
        system.debug(Con.getRecord());
        
        NewAccount = (Account)Con.getRecord();
        Database.SaveResult saveResult = Database.insert(NewAccount, false);
        
             system.debug('Data in duplicateRecords' + duplicateRecords);

        if (!saveResult.isSuccess()) {
            system.debug('IS NOT SUCCESS');
            for (Database.Error error : saveResult.getErrors()) {                
                if (error instanceof Database.DuplicateError) {                    
                    Database.DuplicateError duplicateError = 
                            (Database.DuplicateError)error;
                    Datacloud.DuplicateResult duplicateResult = 
                            duplicateError.getDuplicateResult();
                    
                    // Display duplicate error message as defined in the duplicate rule
                    ApexPages.Message errorMessage = new ApexPages.Message(
                            ApexPages.Severity.ERROR, 'Duplicate Error: ' + 
                            duplicateResult.getErrorMessage());
                    ApexPages.addMessage(errorMessage);
                    
                    // Get duplicate records
                    this.duplicateRecords = new List<sObject>();

                    // Return only match results of matching rules that 
                    //  find duplicate records
                    Datacloud.MatchResult[] matchResults = 
                            duplicateResult.getMatchResults();

                    // Just grab first match result (which contains the 
                    //   duplicate record found and other match info)
                    Datacloud.MatchResult matchResult = matchResults[0];

                    Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();

                    // Add matched record to the duplicate records variable
                    for (Datacloud.MatchRecord matchRecord : matchRecords) {
                        this.duplicateRecords.add((Account)matchRecord.getRecord());                        
                    }
                    this.hasDuplicateResult = !this.duplicateRecords.isEmpty();                         
                    errorDuplicateText =this.duplicateRecords.size() +' Possible Duplicate Records Found.<br/>' + duplicateResult.getErrorMessage();
                }
            }
            //If there’s a duplicate record, stay on the page
            return null;
        }
        
        //  After save, navigate to the view page:
        return (new ApexPages.StandardController(NewAccount)).view();
    }
    
    
}